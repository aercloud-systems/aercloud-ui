use std::any::Any;
use piston_window::G2dTextureContext;
use crate::{
    event::{Event, EventResult},
    align::Alignment,
    printer::Printer,
    text::FontCacheRef,
    vec::PointVec
};

//TODO: separate method for widget focusing?
/// Main trait that defines a GUI widget
pub trait Widget: Any + 'static {
    /// Draw the widget
    ///
    /// This is the only required method to implement
    fn draw(&self, printer: &mut Printer);

    /// Process a window event like keyboard presses or mouse clicks
    /// 
    /// Ignores all events by default
    fn on_event(&mut self, _event: Event, _image_context: &mut G2dTextureContext, _font_cache: FontCacheRef) -> EventResult {
        EventResult::Ignored
    }

    /// Layout the widget according the set size.
    /// This is called once the size has been set for
    /// the widget
    ///
    /// Also called when the window resizes
    fn layout(&mut self, _size: PointVec, _image_context: &mut G2dTextureContext, _font_cache: FontCacheRef) { }

    /// Get the initial size of the widget according to a maximum size
    ///
    /// Also called when the window resizes
    /// 
    /// default size is 1 x 1
    fn size(&self, _max: PointVec, _font_cache: FontCacheRef) -> PointVec { PointVec::new(1., 1.) }

    /// Can this widget be focused by a widget container?
    ///
    /// Returns false by default
    fn can_focus(&self) -> bool { false }

    /// Draw the widget according to specific margins and alignment
    ///
    /// Please don't reimplement this. It's not necessary and will cause problems.
    fn draw_aligned(&self, align: Alignment, printer: &mut Printer) {
        let elem_size = self.size(printer.size, printer.font_cache);
        let orig_offset = printer.offset;
        let orig_size = printer.size;
        printer.add_offset(align.calc(printer.size, elem_size));
        printer.set_size(elem_size);
        printer.stencil_lvl += 1;
        printer.prep_stencil_area();
        self.draw(printer);
        printer.stencil_lvl -= 1;
        printer.set_size(orig_size);
        printer.set_offset(orig_offset);
    }
}

/// The unit type can be used as a dummy widget
impl Widget for () {
    fn draw(&self, _: &mut Printer) { }
}