use rust_utils::{chainable, new_default};
use crate::vec::PointVec;

/// The alignment of a widget inside the window or parent widget
#[derive(Copy, Clone)]
#[chainable]
pub struct Alignment {
    /// Horizontal alignment policy
    #[chainable(doc = "Set the horizontal alignment")]
    pub h_align: HAlignment,

    /// Vertical alignment policy
    #[chainable(doc = "Set the vertical alignment")]
    pub v_align: VAlignment,

    /// Horizontal margin in points
    #[chainable(use_into_impl, doc = "Set the horizontal margin")]
    pub h_margin: f64,

    /// Vertical margin in points
    #[chainable(use_into_impl, doc = "Set the vertical margin")]
    pub v_margin: f64
}

impl Alignment {
    /// Create a new centered [`Alignment`]
    pub fn new() -> Self {
        Alignment {
            h_align: HAlignment::Center,
            v_align: VAlignment::Center,
            h_margin: 0.,
            v_margin: 0.
        }
    }

    pub (super) fn calc(&self, size: PointVec, elem_size: PointVec) -> PointVec {
        PointVec::new(
            self.h_align.calc(*size.x, *elem_size.x, self.h_margin),
            self.v_align.calc(*size.y, *elem_size.y, self.v_margin)
        )
    }
}

new_default!(Alignment);

impl<P: Into<PointVec>> From<P> for Alignment {
    fn from(val: P) -> Self {
        let point = val.into();
        Self::new()
            .h_align(HAlignment::Absolute(point.x.into()))
            .v_align(VAlignment::Absolute(point.y.into()))
    }
}

/// Alignment of a widget on the X axis
#[derive(Copy, Clone)]
pub enum HAlignment {
    /// The left side of the widget is this X value
    Absolute(f64),

    /// Align the widget to the left
    Left,

    /// Center the widget
    Center,

    /// Align the widget to the right
    Right
}

impl HAlignment {
    pub (super) fn calc(self, width: f64, elem_width: f64, margin: f64) -> f64 {
        match self {
            Self::Absolute(x) => x,
            Self::Left => margin,
            Self::Right => width - elem_width - margin,
            Self::Center => (width / 2.) - (elem_width / 2.)
        }
    }
}

/// Alignment of a widget on the Y axis
#[derive(Copy, Clone)]
pub enum VAlignment {
    /// The top side of the widget is this Y value
    Absolute(f64),

    /// Align the widget to the top
    Top,

    /// Center the widget
    Center,

    /// Align the widget to the bottom
    Bottom
}

impl VAlignment {
    pub (super) fn calc(self, height: f64, elem_height: f64, margin: f64) -> f64 {
        match self {
            Self::Absolute(y) => y,
            Self::Top => margin,
            Self::Bottom => height - elem_height - margin,
            Self::Center => (height / 2.) - (elem_height / 2.)
        }
    }
}