use std::fmt::Display;
use rust_utils::{chainable, encapsulated, new_default};
use super::{
    TextStyle,
    StyledSpan,
    StyledSpans,
    StyledSpansMut,
    StyledSpansOwned,
    LinesIterator,
    StyledSpanRef,
    FontCacheRef
};
use crate::{
    vec::PointVec,
    Printer
};

/// A formatted string with styled spans that can
/// optionally be word-wrapped
#[derive(Clone)]
#[encapsulated]
pub struct StyledString {
    spans: Vec<StyledSpan>,

    #[getter(copy_val, doc = "The font size of this [`StyledString`]")]
    #[setter(doc = "Set the font size")]
    #[chainable]
    font_size: u32,

    #[getter(copy_val, doc = "Get the word-wrap width")]
    #[setter(doc = "Set the word-wrap width")]
    #[chainable(collapse_option, use_into_impl)]
    wrap_width: Option<f64>
}

impl StyledString {
    /// Create a new [`StyledString`]
    pub fn new() -> Self {
        StyledString {
            spans: vec![],
            font_size: 15,
            wrap_width: None
        }
    }

    /// Create a plain [`StyledString`] with the specified text
    pub fn plain<T: Display>(text: T) -> Self {
        Self::new()
            .plain_text(text)
    }

    /// Create a [`StyledString`] with the specified text and style
    pub fn styled<S: Into<TextStyle>, T: Display>(style: S, text: T) -> Self {
        Self::new()
            .styled_text(style, text)
    }

    /// Add plain text to this [`StyledString`]
    #[chainable]
    pub fn add_plain_text<T: Display>(&mut self, text: T) {
        self.spans.push(StyledSpan::new(text))
    }

    /// Add styled text to this [`StyledString`]
    #[chainable]
    pub fn add_styled_text<S: Into<TextStyle>, T: Display>(&mut self, style: S, text: T) {
        self.spans.push(StyledSpan::new(text).style(style))
    }

    /// Is this [`StyledString`] empty?
    pub fn is_empty(&self) -> bool { self.spans.is_empty() }

    /// The length of this [`StyledString`] in number of drawable characters
    pub fn len(&self) -> usize {
        self.as_styled_str().len()
    }

    /// The size, in bytes, of the underlying text
    pub fn byte_size(&self) -> usize {
        self.as_styled_str().byte_size()
    }

    /// The drawable size of this [`StyledString`]
    pub fn size(&self, font_cache: FontCacheRef) -> PointVec {
        self.as_styled_str().size(font_cache)
    }

    /// How many lines this [`StyledString`] have?
    pub fn num_lines(&self, font_cache: FontCacheRef) -> usize {
        self.as_styled_str().num_lines(font_cache)
    }

    /// Iterate over all the spans in this [`StyledString`]
    #[must_use = "iterators are lazy and do nothing unless consumed"]
    pub fn iter(&self) -> StyledSpansOwned {
        self.spans.iter()
    }

    /// Mutably iterate over all the spans in this [`StyledString`]
    #[must_use = "iterators are lazy and do nothing unless consumed"]
    pub fn iter_mut(&mut self) -> StyledSpansMut {
        self.spans.iter_mut()
    }

    /// Convert this [`StyledString`] into its borrowed form
    pub fn as_styled_str(&self) -> StyledStr {
        let spans: Vec<StyledSpanRef> = self.spans.iter()
            .map(StyledSpan::as_ref)
            .collect();

        StyledStr {
            spans,
            font_size: self.font_size,
            wrap_width: self.wrap_width
        }
    }

    /// Draw this [`StyledString`] to a widget area
    pub fn draw(&self, pos: PointVec, printer: &mut Printer) {
        self.as_styled_str().draw(pos, printer);
    }
}

impl From<String> for StyledString {
    fn from(text: String) -> Self {
        Self::plain(&text)
    }
}

impl From<&str> for StyledString {
    fn from(text: &str) -> Self {
        Self::plain(text)
    }
}

new_default!(StyledString);

/// The borrowed form of [`StyledString`]
#[derive(Clone)]
#[encapsulated]
pub struct StyledStr<'t> {
    spans: Vec<StyledSpanRef<'t>>,

    #[getter(copy_val, doc = "The font size of this [`StyledStr`]")]
    #[setter(doc = "Set the font size")]
    font_size: u32,

    #[getter(copy_val, doc = "Get the word-wrap width")]
    #[setter(doc = "Set the word-wrap width")]
    #[chainable(collapse_option, doc =
        "Set the word-wrap width\n\
        \n\
        Chainable variant"
    )]
    wrap_width: Option<f64>
}

impl<'t> StyledStr<'t> {
    /// Is this [`StyledStr`] empty?
    pub fn is_empty(&self) -> bool { self.spans.is_empty() }

    /// The length of this [`StyledStr`] in number of drawable characters
    pub fn len(&self) -> usize {
        self.spans.iter()
            .map(StyledSpanRef::len)
            .sum()
    }

    /// The size, in bytes, of the underlying text
    pub fn byte_size(&self) -> usize {
        self.spans.iter()
            .map(|span| span.text.len())
            .sum()
    }

    /// The drawable size of this [`StyledStr`]
    pub fn size(&self, font_cache: FontCacheRef) -> PointVec {
        LinesIterator::new(self, self.font_size, self.wrap_width, font_cache)
            .size(font_cache)
    }

    /// How many lines does this [`StyledStr`] have?
    pub fn num_lines(&self, font_cache: FontCacheRef) -> usize {
        LinesIterator::new(self, self.font_size, self.wrap_width, font_cache)
            .count()
    }

    /// Draw this [`StyledStr`] to a widget area
    pub fn draw(&self, mut pos: PointVec, printer: &mut Printer) {
        let spacing = self.font_size as f64 / 5.;
        let lines = LinesIterator::new(self, self.font_size, self.wrap_width, printer.font_cache);

        for line in lines {
            line.draw(pos + (spacing, spacing), printer, self.font_size);
            pos += (0, line.size(printer.font_cache, self.font_size).y + spacing);
        }
    }

    /// Convert this [`StyledStr`] into a [`StyledString`]
    pub fn to_styled_string(&self) -> StyledString {
        let spans: Vec<StyledSpan> = self.spans.iter()
            .map(StyledSpanRef::to_span)
            .collect();

        StyledString {
            spans,
            font_size: self.font_size,
            wrap_width: self.wrap_width
        }
    }

    /// Iterate over all the spans in this [`StyledStr`]
    #[must_use = "iterators are lazy and do nothing unless consumed"]
    pub fn iter(&self) -> StyledSpans {
        self.spans.iter().copied()
    }
}

impl<'t> From<&'t str> for StyledStr<'t> {
    fn from(text: &'t str) -> Self {
        StyledStr {
            spans: vec![
                StyledSpanRef {
                    text,
                    style: None
                }
            ],

            font_size: 15,
            wrap_width: None
        }
    }
}

impl<'t> From<&'t String> for StyledStr<'t> {
    fn from(text: &'t String) -> Self {
        From::<&'t str>::from(text)
    }
}

impl<'t> From<&'t StyledString> for StyledStr<'t> {
    fn from(text: &'t StyledString) -> Self {
        text.as_styled_str()
    }
}

/// A trait that allows conversion of [`Display`] implementations into [`StyledString`]s
///
/// Just like with [`String`], any object implementing [`Display`]
/// can be converted to a [`StyledString`]
pub trait ToStyledString: Display {
    /// Convert the given value to a [`StyledString`]
    /// 
    /// # Panics
    ///
    /// This method will panic if the [`Display`] implementation returns an error
    fn to_styled_string(&self) -> StyledString {
        self.to_string().into()
    }
}

impl<T: Display> ToStyledString for T { }