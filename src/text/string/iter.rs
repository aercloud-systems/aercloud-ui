use super::StyledSpanRef;
use std::iter::FusedIterator;
use crate::{
    text::{
        StyledStr,
        FontCacheRef
    },
    vec::PointVec,
    Printer
};

/// A single line of wordwrapped text with line breaks
#[derive(Clone)]
pub struct Line<'t>(Vec<StyledSpanRef<'t>>);

impl<'t> Line<'t> {
    /// Draw this [`Line`] to a widget area
    pub fn draw(&self, mut pos: PointVec, printer: &mut Printer, font_size: u32) {
        for span in self.0.iter() {
            let span_width = span.size(printer.font_cache, font_size).x;
            span.draw(pos, printer, font_size);
            pos.x += span_width;
        }
    }

    /// The drawable size of this [`Line`]
    pub fn size(&self, font_cache: FontCacheRef, font_size: u32) -> PointVec {
        let width: f64 = self.0.iter()
            .map(|span| *(span.size(font_cache, font_size).x))
            .sum();

        let height = self.0.iter()
            .map(|span| span.size(font_cache, font_size).y)
            .max().unwrap_or(0.into());

        (width, height).into()
    }

    pub (super) fn new() -> Self {
        Line(vec![])
    }

    pub (super) fn add_span(&mut self, span: StyledSpanRef<'t>) {
        self.0.push(span);
    }

    pub (super) fn clear(&mut self) { self.0.clear(); }

    fn merge(&mut self, other: Line<'t>) {
        for span in &other.0 {
            self.0.push(*span);
        }
    }

    fn remove_trailing_whitespace(&mut self) {
        self.0.reverse();

        let mut new_raw_line = Vec::new();
        let mut is_trailing_space = true;
        for span in &self.0 {
            if span.text != " " && is_trailing_space {
                is_trailing_space = false;
            }
            if span.text != " " || !is_trailing_space {
                new_raw_line.push(*span)
            }
        }

        new_raw_line.reverse();
        self.0 = new_raw_line;
    }
}

impl<'t, L: IntoIterator<Item = StyledSpanRef<'t>>> From<L> for Line<'t> {
    fn from(raw_line: L) -> Self {
        Line(raw_line.into_iter().collect())
    }
}

/// An iterator over the lines of wordwrapped text with line breaks
#[must_use = "iterators are lazy and do nothing unless consumed"]
#[derive(Clone)]
pub struct LinesIterator<'t> {
    lines: Vec<Line<'t>>,
    font_size: u32
}

impl<'t> LinesIterator<'t> {
    /// Create a new [`LinesIterator`]
    pub fn new(source: &'t StyledStr, font_size: u32, max_width: Option<f64>, font_cache: FontCacheRef) -> Self {
        let mut cur_line = Line::new();
        let mut lines = Vec::new();
        for span in source.iter() {
            if let Some(max_width) = max_width {
                let mut rem_width = max_width - *(cur_line.size(font_cache, font_size).x);
                for line in span.split_newlines() {
                    let line_width = *(line.size(font_cache, font_size).x);
                    if line_width > rem_width {
                        let mut lines_to_add = line.split_wordwrapped(rem_width, max_width, font_cache, font_size);
                        let first_line = lines_to_add.next().unwrap_or(Line::new());
                        cur_line.merge(first_line);
                        lines.push(cur_line.clone());
                        lines.extend(lines_to_add);
                        cur_line = lines.pop().unwrap_or(Line::new());
                        rem_width = max_width - *(cur_line.size(font_cache, font_size).x);
                    }
                    else {
                        cur_line.add_span(line);
                        rem_width -= line_width;
                    }

                    if span.text.contains('\n') {
                        lines.push(cur_line.clone());
                        cur_line.clear();
                        rem_width = max_width;
                    }
                }
            }
            else {
                for line in span.split_newlines() {
                    cur_line.add_span(line);
                    if span.text.contains('\n') {
                        lines.push(cur_line.clone());
                        cur_line.clear();
                    }
                }
            }
        }

        lines.push(cur_line);

        for line in &mut lines {
            line.remove_trailing_whitespace();
        }

        LinesIterator {
            lines,
            font_size
        }
    }

    /// The drawable size of this [`LinesIterator`]
    pub fn size(&self, font_cache: FontCacheRef) -> PointVec {
        let spacing = self.font_size as f64 / 5.;

        let sizes: Vec<PointVec> = self.lines.iter()
            .map(|span| span.size(font_cache, self.font_size))
            .collect();

        let height: f64 = sizes.iter()
            .map(|size| *size.y + spacing)
            .sum();

        let width = sizes.iter()
            .map(|size| size.x)
            .max()
            .unwrap();

        (width + (spacing * 2.), height + spacing).into()
    }
}

impl<'t> Iterator for LinesIterator<'t> {
    type Item = Line<'t>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.lines.is_empty() {
            None
        }
        else {
            Some(self.lines.remove(0))
        }
    }
}

impl<'t> FusedIterator for LinesIterator<'t> { }