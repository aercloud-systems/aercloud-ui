use super::{
    TextStyle,
    FontCacheRef,
    FontType
};
use crate::{
    vec::{PointVec, RawPointVec},
    theme::PaletteColor,
    Printer
};
use std::{
    iter::Copied,
    fmt::Display,
    vec::IntoIter,
    slice::{Iter, IterMut}
};
use piston_window::{
    CharacterCache,
    Text,
    Transformed
};
use rust_utils::chainable;

mod styled;
mod iter;

pub use styled::*;
pub use iter::*;
use unicode_width::UnicodeWidthStr;

/// Iterator over the spans in a [`StyledStr`]
pub type StyledSpans<'s, 't> = Copied<Iter<'s, StyledSpanRef<'t>>>;

/// Mutable iterator over the spans in a [`StyledString`]
pub type StyledSpansMut<'s> = IterMut<'s, StyledSpan>;

/// Iterator over the spans in a [`StyledString`]
pub type StyledSpansOwned<'s> = Iter<'s, StyledSpan>;

/// A single text span with an optional style
#[derive(Clone)]
#[chainable]
pub struct StyledSpan {
    /// The style of this span, if there is one
    #[chainable(collapse_option, use_into_impl, doc = "Add a style to the span of text")]
    pub style: Option<TextStyle>,

    /// The text of this span
    pub text: String
}

impl StyledSpan {
    /// Create a new plain [`StyledSpan`]
    pub fn new<T: Display>(text: T) -> Self {
        StyledSpan {
            style: None,
            text: text.to_string()
        }
    }

    /// Get the drawable size of this [`StyledSpan`] with the specified font size
    pub fn size(&self, font_cache: FontCacheRef, font_size: u32) -> PointVec {
        self.as_ref().size(font_cache, font_size)
    }

    /// Draw the span at the specified position
    pub fn draw(&self, pos: PointVec, printer: &mut Printer, font_size: u32) {
        self.as_ref().draw(pos, printer, font_size)
    }

    /// The length of this [`StyledSpan`] in drawable characters
    pub fn len(&self) -> usize { self.text.width() }

    /// Split this [`StyledSpan`] into 2 references at the specified  character index
    pub fn split_at(&self, char_idx: usize) -> (StyledSpanRef, StyledSpanRef) {
        self.as_ref().split_at(char_idx)
    }

    /// Create a [`StyledSpanRef`] from this [`StyledSpan`]
    pub fn as_ref(&self) -> StyledSpanRef {
        StyledSpanRef {
            style: self.style,
            text: &self.text
        }
    }

    /// Is this an empty span?
    pub fn is_empty(&self) -> bool { self.text.is_empty() }
}

/// The borrowed form of [`StyledSpan`]. Can be a reference to a [`StyledSpan`] or part of one.
#[derive(Copy, Clone)]
pub struct StyledSpanRef<'t> {
    /// The style of this span, if there is one
    pub style: Option<TextStyle>,

    /// The text of this span
    pub text: &'t str
}

impl<'t> StyledSpanRef<'t> {
    /// Get the drawable size of this [`StyledSpanRef`] with the specified font size
    pub fn size(&self, font_cache: FontCacheRef, font_size: u32) -> PointVec {
        if self.is_empty() { return PointVec::new(0, 0); }

        let style = if let Some(style) = self.style {
            style
        }
        else { TextStyle::new() };

        let mut font_supports_style = false;
        let font_type = FontType::from_format_flags(style.bold, style.italic);
        let font = if let Some(font) = font_cache.font_mut(font_type) {
            font_supports_style = true;
            font
        }
        else { font_cache.default_font_mut() };

        let mut height = font_size as f64;
        let mut width = font.width(font_size, &self.text).unwrap_or(0.);

        if !font_supports_style {
            if style.bold {
                height += 4.;
                width += 4.;
            }

            if style.italic && !style.bold {
                width += 4.
            }
        }

        if style.underline {
            height += 2.;
        }

        (width, height).into()
    }

    /// Draw the span at the specified position
    pub fn draw(&self, pos: PointVec, printer: &mut Printer, font_size: u32) {
        if self.is_empty() { return; }
        let state = printer.get_draw_state();

        let style = if let Some(style) = self.style {
            style
        }
        else {
            printer.theme.palette[PaletteColor::Primary].into()
        };

        let size = self.size(printer.font_cache, font_size);
        printer.draw_box(pos, size, style.style.back, None);
        if style.underline {
            printer.draw_line(pos + (0, size.y), pos + (size.x, size.y), 0.5, style.style.front);
        }
        let mut font_supports_style = false;
        let font_type = FontType::from_format_flags(style.bold, style.italic);
        let font = if let Some(font) = printer.font_cache.font_mut(font_type) {
            font_supports_style = true;
            font
        }
        else { printer.font_cache.default_font_mut() };

        let text_to_draw = Text::new_color(style.style.front.into(), font_size);
        let raw_text_pos: RawPointVec = [pos.x.into(), (pos.y + font_size as f64).into()];

        if font_supports_style {
            text_to_draw.draw_pos(self.text, raw_text_pos, font, &state, printer.ctx.transform, printer.gfx)
                .expect("Unable to render text!");
        }
        else {
            let transform = if style.italic {
                printer.ctx.transform
                    .shear(2., 0.)
            }
            else {
                printer.ctx.transform
            };

            if style.bold {
                let local_offsets = [(2., 2.), (2., -2.), (-2., 2.), (-2., -2.)];

                for (ofs_x, ofs_y) in local_offsets {
                    text_to_draw.draw_pos(self.text, raw_text_pos, font, &state, transform.trans(ofs_x + 2., ofs_y + 2.), printer.gfx)
                        .expect("Unable to render text!");    
                }
            }
            else {
                text_to_draw.draw_pos(self.text, raw_text_pos, font, &state, transform, printer.gfx)
                    .expect("Unable to render text!");
            }
        }
    }

    /// Split this [`StyledSpanRef`] into 2 at the specified  character index
    pub fn split_at(&self, char_idx: usize) -> (Self, Self) {
        let text1 = &self.text[..char_idx];
        let text2 = &self.text[char_idx..];

        (
            StyledSpanRef {
                style: self.style,
                text: text1
            },

            StyledSpanRef {
                style: self.style,
                text: text2
            }
        )
    }

    /// Split this [`StyledSpanRef`] by line and iterate over it
    pub fn split_newlines(&self) -> impl Iterator<Item = Self> + '_ {
        self.text.lines()
            .map(|text| {
                StyledSpanRef {
                    style: self.style,
                    text
                }
            })
    }

    /// Split this [`StyledSpanRef`] by the spaces and iterate over it
    /// 
    /// the tuple should be interpreted as `(has_trailing_space, span)`
    pub fn split_spaces(&self) -> impl Iterator<Item = (bool, Self)> + '_ {
        let has_trailing_space = self.text.ends_with(' ');
        let mut wc = self.text.split(' ').count();
        let mut space_split = self.text.split(' ');

        if has_trailing_space {
            space_split.next_back();
            wc -= 1;
        }

        space_split
            .map(|text| {
                StyledSpanRef {
                    style: self.style,
                    text
                }
            })
            .enumerate()
            .map(move |(i, span)| {
                if i < wc - 1 {
                    (true, span)
                }
                else {
                    (has_trailing_space, span)
                }
            })
    }

    /// The length of this [`StyledSpanRef`] in drawable characters
    pub fn len(&self) -> usize { self.text.width() }

    /// Create a new [`StyledSpan`] from this [`StyledSpanRef`]
    pub fn to_span(&self) -> StyledSpan {
        StyledSpan {
            style: self.style,
            text: self.text.to_string()
        }
    }

    /// Is this an empty span?
    pub fn is_empty(&self) -> bool { self.text.is_empty() }

    fn split_wordwrapped(&self, mut rem_width: f64, max_width: f64, font_cache: FontCacheRef, font_size: u32) -> IntoIter<Line<'t>> {
        let mut lines = Vec::new();
        let mut cur_line = Line::new();

        for (has_trailing_space, word) in self.split_spaces() {
            let space = StyledSpanRef {
                style: word.style,
                text: " "
            };

            let width = *(word.size(font_cache, font_size).x) + 
                if has_trailing_space {
                    *(space.size(font_cache, font_size).x)
                }
                else { 0. };

            if width > rem_width {
                lines.push(cur_line.clone());
                cur_line.clear();
                rem_width = max_width;
            }

            cur_line.add_span(word);

            if has_trailing_space {
                cur_line.add_span(space);
            }

            rem_width -= width;
        }

        lines.push(cur_line);
        lines.into_iter()
    }
}