use crate::theme::ColorStyle;
use rust_utils::{new_default, chainable};

/// The style for a span of text
#[chainable]
#[derive(Copy, Clone)]
pub struct TextStyle {
    /// The span's [`ColorStyle`]
    #[chainable(use_into_impl, doc = "Set the color style")]
    pub style: ColorStyle,

    /// Bold text
    #[chainable(doc = "Bold text")]
    pub bold: bool,

    /// Italic text
    #[chainable(doc = "Italic text")]
    pub italic: bool,

    /// Underlined text
    #[chainable(doc = "Underlined text")]
    pub underline: bool
}

impl TextStyle {
    /// Create a new [`TextStyle`]
    pub fn new() -> Self {
        TextStyle {
            style: ColorStyle::new(),
            bold: false,
            italic: false,
            underline: false
        }
    }
}

impl<T: Into<ColorStyle>> From<T> for TextStyle {
    fn from(color_style: T) -> Self {
        TextStyle {
            style: color_style.into(),
            bold: false,
            italic: false,
            underline: false
        }
    }
}

new_default!(TextStyle);