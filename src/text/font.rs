use std::{
    collections::HashMap,
    hash::Hash,
    path::Path
};
use gfx_device_gl::Device;
use piston_window::{
    Character,
    CharacterCache,
    G2dTexture,
    PistonWindow
};
use rust_utils::utils;

pub use piston_window::Glyphs as Font;

/// A renderable character in a font
pub type FontCharacter<'f> = Character<'f, G2dTexture>;

/// Mutable reference to the font cache
pub type FontCacheRef<'f> = &'f mut FontCache;

/// The type of font variant in the [`FontCache`]
#[derive(Clone, Copy, Hash, PartialEq, Eq, Debug)]
pub enum FontType {
    Regular,
    Bold,
    Italic,
    BoldItalic
}

impl FontType {
    /// All the font types as an array
    pub fn values() -> impl Iterator<Item = Self> {
        [Self::Regular, Self::Bold, Self::Italic, Self::BoldItalic].into_iter()
    }

    /// Get a specific [`FontType`] from the formatting flags
    pub fn from_format_flags(bold: bool, italic: bool) -> Self {
        if bold && italic {
            Self::BoldItalic
        }
        else if bold {
            Self::Bold
        }
        else if italic {
            Self::Italic
        }
        else {
            Self::Regular
        }
    }

    // is the primary style of this font this font type?
    fn is_font_this(self, styles_raw: &str) -> bool {
        match self {
            // a font is considered a "regular" font if it is none of the 3 types below
            Self::Regular => !(
                styles_raw.contains("style=Bold") ||
                styles_raw.contains("style=Italic") ||
                styles_raw.contains("style=Bold Italic") ||
                styles_raw.contains("style=BoldItalic")
            ),

            Self::Bold => styles_raw.contains("style=Bold"),
            Self::Italic => styles_raw.contains("style=Italic"),
            Self::BoldItalic => styles_raw.contains("style=Bold Italic") || styles_raw.contains("style=BoldItalic")
        }
    }
}

/// A font with all its variants (bold, italic, etc.)
pub struct FontCache {
    fonts: HashMap<FontType, Font>
}

impl FontCache {
    /// Load a font from the system's installed fonts by name. This calls `fc-list` to find the font
    /// and all its variants
    ///
    /// The Noto Sans font will be loaded if the specified font cannot be found
    pub fn load(window: &mut PistonWindow, font_name: &str) -> Self {
        let fc_list_output = utils::run_command("fc-list", false, [""]).output;
        let mut fonts = HashMap::new();

        for line in fc_list_output.lines() {
            let mut line_split = line.split(':');
            let path = line_split.next().unwrap_or("");
            let name_raw = line_split.next().unwrap_or("");
            let styles_raw = line_split.next().unwrap_or("");
            if path.is_empty() || name_raw.is_empty() || styles_raw.is_empty() {
                continue;
            }

            if &name_raw[1..] == font_name {
                for font_type in FontType::values() {
                    if font_type.is_font_this(styles_raw) {
                        if !fonts.contains_key(&font_type) {
                            fonts.insert(font_type, window.load_font(path).expect("Unable to load font!"));
                        }
                    }
                }
            }
        }

        if fonts.is_empty() && font_name == "Noto Sans" {
            panic!("The Noto Sans font is not installed!")
        }
        else if fonts.is_empty() {
            Self::load(window, "Noto Sans")
        }
        else {
            FontCache { fonts }
        }
    }

    /// Load a font from the specified path
    ///
    /// Panics if the font cannot be found
    pub fn load_from_path<P: AsRef<Path>>(window: &mut PistonWindow, font_path: P) -> Self {
        let mut fonts = HashMap::new();
        fonts.insert(FontType::Regular, window.load_font(font_path).unwrap());
        FontCache { fonts }
    }

    /// Return the width for some given text with the specified [`FontType`].
    pub fn width(&mut self, mut font_type: FontType, font_size: u32, text: &str) -> f64 {
        if self.font_mut(font_type).is_none() {
            font_type = FontType::Regular
        }

        let font = self.font_mut(font_type).unwrap();
        font.width(font_size, text).unwrap_or(0.)
    }

    /// Get reference to a character for the specified [`FontType`].
    pub fn character(&mut self, mut font_type: FontType, font_size: u32, ch: char) -> Option<FontCharacter> {
        if self.font_mut(font_type).is_none() {
            font_type = FontType::Regular
        }

        let font = self.font_mut(font_type).unwrap();
        font.character(font_size, ch).ok()
    }

    /// Reference to a specific font
    pub fn font(&self, font_type: FontType) -> Option<&Font> {
        self.fonts.get(&font_type)
    }

    /// Mutable reference to a specific font
    pub fn font_mut(&mut self, font_type: FontType) -> Option<&mut Font> {
        self.fonts.get_mut(&font_type)
    }

    /// Reference to the default font
    pub fn default_font(&self) -> &Font {
        self.font(FontType::Regular).unwrap()
    }

    /// Mutable reference to the default font
    pub fn default_font_mut(&mut self) -> &mut Font {
        self.font_mut(FontType::Regular).unwrap()
    }

    pub (crate) fn flush_buffer(&mut self, dev: &mut Device) {
        for font in self.fonts.values_mut() {
            font.factory.encoder.flush(dev)
        }
    }
}