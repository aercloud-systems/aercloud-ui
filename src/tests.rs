use crate::{
    widgets::{
        ImageWidget,
        SelectWidget,
        TextWidget
    },
    theme::{BaseColor, Color},
    text::StyledString,
    hlayout, vlayout,
    AercloudUI
};

#[test]
fn widget_gallery() {
    //let aqua = Color::RGBA(67, 167, 167, 255);
    //let silver = Color::Dark(BaseColor::White).alpha(64);
    let mut ui = AercloudUI::new_with_icon("Widget Gallery", "icon.png")
        .font("Monocraft")
        .debug(true);

    let coof_img = ImageWidget::new("coof.png", ui.img_ctx());

    ui.add_layer(
        hlayout!(
            vlayout!(
                TextWidget::new("Aercloud UI").font_size(50).word_wrap(true), 
                TextWidget::new("The coof virus").font_size(50).word_wrap(true),
                coof_img,
                TextWidget::new(
                    StyledString::styled(Color::Light(BaseColor::Red), "COVID-19 ")
                        .plain_text("can be deadly to some people, but a mild ")
                        .styled_text(Color::Light(BaseColor::Green), "infection")
                        .plain_text(" to others")
                        .wrap_width(500.)
                )
                    .word_wrap(true)
            ).fill(true),

            SelectWidget::new()
                .item("Red", Color::Light(BaseColor::Red))
                .item("White", Color::Light(BaseColor::White))
                .item("Blue", Color::Light(BaseColor::Blue))
                .item("Default Color", Color::Transparent.alpha(192))
                .on_submit(|ui, color| ui.set_backdrop_color(*color)),

            SelectWidget::new()
                .item("Snowy Desert", "backdrops/snowy_desert.jpg")
                .item("Sunset Point", "backdrops/sunset_point.jpg")
                .item("Arizona Sunset", "backdrops/sunset.jpg")
                .on_submit(|ui, path| ui.set_backdrop_image(*path))
        ).fill(true)
    );

    ui.run();
}

#[test]
fn text_formatting() {
    
}