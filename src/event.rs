use std::{
    cell::RefCell,
    fmt::{
        Debug,
        Formatter,
        Result as FmtResult
    },
    ops::Deref,
    rc::Rc
};

pub use piston_window::{MouseButton, Key};

/// A raw event from the piston API
pub use piston_window::Event as PistonEvent;

use crate::{
    align::Alignment,
    text::FontCache,
    vec::PointVec,
    AercloudUI, Widget
};

/// An event (like a keyboard press or mouse click)
/// that can be sent to a widget
//TODO: multiple keys pressed event?
#[derive(Copy, Clone, PartialEq, Eq)]
pub enum Event {
    /// A key was pressed
    Key(Key),

    /// A mouse event occurred
    Mouse {
        /// The offset of the widget the mouse cursor is in
        offset: PointVec,

        /// The position of the cursor relative to `offset`
        pos: PointVec,

        /// The mouse event
        event: MouseEvent
    },

    /// A refresh event has occurred
    Refresh,

    /// Request for focusing a widget
    ///
    /// This is normally propragated to widgets by widget containers
    /// like `LinearLayout`s that handle multiple child widgets
    Focus(FocusDirection),

    /// The widget is no longer focused
    ///
    /// This is normally propragated to widgets by widget containers
    /// like `LinearLayout`s that handle multiple child widgets
    FocusLost
}

impl Event {
    /// If this is a mouse event, its offset is set to the offset of the passed widget
    /// with the specified alignment and parent size
    ///
    /// This will return Event::Refresh if the mouse position is not within the widget bounds
    pub fn mouse_offset<W: Widget + ?Sized>(self, align: Alignment, parent_size: PointVec, widget: &W, font_cache: &mut FontCache) -> Self {
        let elem_size = widget.size(parent_size, font_cache);
        let ofs = align.calc(parent_size, elem_size);
        let bottom_corner = ofs + elem_size;

        if let Event::Mouse { pos, event, .. } = self {
            if ofs <= pos && pos <= bottom_corner {
                Event::Mouse { offset: ofs, pos, event }
            }
            else {
                Event::Refresh
            }
        }
        else { self }
    }

    /// Is this event a pressed arrow key?
    pub fn is_arrow_key(self) -> bool {
        matches!(self, Event::Key(Key::Left | Key::Right | Key::Up | Key::Down))
    }

    /// If this is a mouse event, returns the widget-relative mouse coordinates,
    /// otherwise it returns the origin
    pub fn local_mouse_coords(self) -> PointVec {
        if let Event::Mouse { offset, pos, .. } = self {
            pos - offset
        }
        else { PointVec::new(0, 0)}
    }
}

impl Debug for Event {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match *self {
            Self::Key(key) => write!(f, "Key pressed: {key:?}"),

            Self::Mouse {
                offset,
                pos,
                event
            } => write!(f, "Mouse event:\nCursor offset: {offset:?}\nCursor position: {pos:?}\n{event:?}"),

            Self::Refresh => write!(f, "Refresh event"),
            Self::Focus(direction) => write!(f, "Focus from direction: {direction:?}"),
            Self::FocusLost => write!(f, "The widget lost focus")
        }
    }
}

/// The direction the widget is being focused from
#[derive(Copy, Clone, PartialEq, Eq)]
pub enum FocusDirection {
    Up,
    Down,
    Left,
    Right,
    None
}

impl FocusDirection {
    /// Get the opposing direction
    pub fn opposite(self) -> Self {
        match self {
            Self::Left => Self::Right,
            Self::Up => Self::Down,
            Self::Right => Self::Left,
            Self::Down => Self::Up,
            Self::None => Self::None
        }
    }
}

impl Debug for FocusDirection {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Self::Up => write!(f, "Up"),
            Self::Down => write!(f, "Down"),
            Self::Left => write!(f, "Left"),
            Self::Right => write!(f, "Right"),
            Self::None => write!(f, "None")
        }
    }
}

/// A mouse event (right/left click, scrolling, etc.)
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum MouseEvent {
    /// A button was pressed
    Press(MouseButton),

    /// A button was released
    Release(MouseButton),

    /// A button is being held down
    Hold(MouseButton),

    /// The scroll wheel has moved up
    WheelUp,

    /// The scroll wheel has moved down
    WheelDown,

    /// The cursor has moved
    CursorMoved
}

impl Debug for MouseEvent {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match *self {
            Self::Press(button) => write!(f, "Button pressed: {button:?}"),
            Self::Hold(button) => write!(f, "Button held: {button:?}"),
            Self::Release(button) => write!(f, "Button released: {button:?}"),
            Self::WheelDown => write!(f, "Scroll down"),
            Self::WheelUp => write!(f, "Scroll up"),
            Self::CursorMoved => write!(f, "The Cursor moved")
        }
    }
}

/// The result of an event (like a keyboard press or mouse click)
/// being sent to a widget
/// 
/// It can be consumed or ignored
#[derive(Clone)]
pub enum EventResult {
    /// The widget ignored the event
    Ignored,

    /// The widget consumed the event
    Consumed(Option<EventCallback>)
}

impl EventResult {
    /// Convenient method to create `Consumed(Some(func))`
    pub fn with_cb<F: Fn(&mut AercloudUI) + 'static>(func: F) -> Self {
        EventResult::Consumed(Some(EventCallback::new(func)))
    }

    /// Convenient method to create `Consumed(Some(func))`
    ///
    /// After being called once, the callback does nothing.
    pub fn with_cb_once<F: FnOnce(&mut AercloudUI) + 'static>(func: F) -> Self {
        EventResult::Consumed(Some(EventCallback::from_fn_once(func)))
    }

    /// Convenient method to create `Consumed(None)`
    pub fn consumed() -> Self {
        EventResult::Consumed(None)
    }

    /// Returns `true` if `self` is `EventResult::Consumed`.
    pub fn is_consumed(&self) -> bool {
        matches!(*self, EventResult::Consumed(_))
    }

    /// Returns `true` if `self` contains a callback.
    pub fn has_callback(&self) -> bool {
        matches!(*self, EventResult::Consumed(Some(_)))
    }

    /// Process this result if it is a callback.
    ///
    /// Does nothing otherwise.
    pub fn process(self, ui: &mut AercloudUI) {
        if let EventResult::Consumed(Some(cb)) = self {
            cb(ui);
        }
    }

    /// Returns `self` if it is not `EventResult::Ignored`, otherwise returns `func()`.
    #[must_use]
    pub fn or_else<F: FnOnce() -> EventResult>(self, func: F) -> Self {
        match self {
            EventResult::Ignored => func(),
            other => other,
        }
    }

    /// Returns an event result that combines `self` and `other`.
    #[must_use]
    pub fn and(self, other: Self) -> Self {
        match (self, other) {
            (EventResult::Ignored, result) | (result, EventResult::Ignored) => result,
            
            (
                EventResult::Consumed(None),
                EventResult::Consumed(cb)
            )
            | (
                EventResult::Consumed(cb),
                EventResult::Consumed(None)
            ) =>
                EventResult::Consumed(cb),

            (
                EventResult::Consumed(Some(cb1)),
                EventResult::Consumed(Some(cb2)),
            ) => 
                EventResult::with_cb(move |ui| {
                    cb1(ui);
                    cb2(ui);
                }),
        }
    }
}

/// A callback produced by a widget consuming an event
/// 
/// Callbacks are ran by the [`AercloudUI`] root as soon as [`Widget::on_event()`] returns
#[derive(Clone)]
pub struct EventCallback(Rc<dyn Fn(&mut AercloudUI)>);

impl EventCallback {
    /// Create a new `EventCallback` from a `Fn(&mut AercloudUI)` closure/function
    pub fn new<F: Fn(&mut AercloudUI) + 'static>(func: F) -> Self {
        EventCallback(Rc::new(func))
    }

    /// Create a new `EventCallback` from a `FnMut(&mut AercloudUI)` closure/function
    ///
    /// If the function tries to call itself, it will do nothing
    pub fn from_fn_mut<F: FnMut(&mut AercloudUI) + 'static>(func: F) -> Self {
        let callback = RefCell::new(func);

        Self::new(move |ui| {
            if let Ok(mut func) = callback.try_borrow_mut() {
                (&mut *func)(ui);
            }
        })
    }

    /// Create a new `EventCallback` from a `FnMut(&mut AercloudUI)` closure/function
    ///
    /// If the function is called more than once, it will do nothing
    pub fn from_fn_once<F: FnOnce(&mut AercloudUI) + 'static>(func: F) -> Self {
        let mut callback = Some(func);

        Self::from_fn_mut(move |ui| {
            if let Some(func) = callback.take() {
                func(ui);
            }
        })
    }
}

impl<F: Fn(&mut AercloudUI) + 'static> From<F> for EventCallback {
    /// Create a new `EventCallback` from a `Fn(&mut AercloudUI)` closure/function
    fn from(func: F) -> Self {
        Self::new(func)
    }
}

impl Deref for EventCallback {
    type Target = dyn Fn(&mut AercloudUI);

    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}