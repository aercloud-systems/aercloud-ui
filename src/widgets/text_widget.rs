use piston_window::G2dTextureContext;
use rust_utils::chainable;
use crate::{
    text::{
        FontCacheRef,
        StyledString
    },
    theme::PaletteColor,
    vec::PointVec,
    Printer, Widget
};

/// A simple widget showing fixed text.
pub struct TextWidget {
    text: StyledString,
    word_wrap: bool,
}

impl TextWidget {
    pub fn new<T: Into<StyledString>>(text: T) -> Self {
        TextWidget {
            text: text.into().font_size(15u32),
            word_wrap: false
        }
    }

    #[chainable]
    pub fn set_font_size(&mut self, font_size: u32) {
        self.text.set_font_size(font_size);
    }

    #[chainable]
    pub fn set_word_wrap(&mut self, word_wrap: bool) {
        if !word_wrap {
            self.text.set_wrap_width(None);
        }
        self.word_wrap = word_wrap;
    }
}

impl Widget for TextWidget {
    fn draw(&self, printer: &mut Printer) {
        printer.set_background(printer.theme.palette[PaletteColor::Widget]);
        printer.draw_text((0, 0), &self.text);
    }

    fn size(&self, max: PointVec, font_cache: FontCacheRef) -> PointVec {
        let text_size = self.text.size(font_cache);
        let new_size = if text_size.x > max.x && self.word_wrap {
            self.text.as_styled_str()
                .wrap_width(*max.x)
                .size(font_cache)
        }
        else {
            text_size
        };

        new_size
    }

    fn layout(&mut self, size: PointVec, _image_context: &mut G2dTextureContext, _font_cache: FontCacheRef) {
        if self.text.get_wrap_width().is_none() && self.word_wrap {
            self.text.set_wrap_width(Some(*size.x))
        }
    }
}