use crate::{
    event::{
        Event, EventResult,
        Key, FocusDirection
    },
    text::{
        FontCacheRef,
        StyledStr,
        StyledString
    },
    theme::PaletteColor,
    vec::PointVec,
    AercloudUI, Printer,
    Widget
};
use std::{
    fmt::Display,
    rc::Rc
};
use piston_window::G2dTextureContext;
use rust_utils::{chainable, new_default};

type SelectCallback<T> = Rc<dyn Fn(&mut AercloudUI, &T)>;

/// Widget to select an item among a list.
///
/// It contains a list of values of type T, with associated labels.
pub struct SelectWidget<T = String>
    where T: 'static
{
    selected_idx: usize,
    items: Vec<Item<T>>,
    font_size: u32,
    on_select: Option<SelectCallback<T>>,
    on_submit: Option<SelectCallback<T>>,
    focused: bool
}

impl SelectWidget {
    #[chainable]
    pub fn add_item_str<S: Display>(&mut self, label: S) {
        self.add_item(label.to_string(), label.to_string());
    }
}

impl<T> SelectWidget<T> {
    #[must_use]
    pub fn new() -> Self {
        SelectWidget {
            selected_idx: 0,
            items: vec![],
            font_size: 15,
            on_select: None,
            on_submit: None,
            focused: false
        }
    }

    #[chainable]
    pub fn set_on_submit<F: Fn(&mut AercloudUI, &T) + 'static>(&mut self, func: F) {
        self.on_submit = Some(Rc::new(func));
    }

    #[chainable]
    pub fn set_on_select<F: Fn(&mut AercloudUI, &T) + 'static>(&mut self, func: F) {
        self.on_select = Some(Rc::new(func));
    }

    #[chainable]
    pub fn set_font_size(&mut self, font_size: u32) {
        self.font_size = font_size;
    }

    #[chainable]
    pub fn add_item<L: Into<StyledString>>(&mut self, label: L, item: T) {
        self.items.push(Item::new(label.into().font_size(self.font_size), item));
    }

    pub fn selected_index(&self) -> usize { self.selected_idx }

    pub fn selected_label(&self) -> Option<StyledStr> {
        Some(
            self.items.get(self.selected_idx)?.label
                .as_styled_str()
        )
    }

    pub fn selected_label_mut(&mut self) -> Option<&mut StyledString> {
        Some(
            &mut self.items.get_mut(self.selected_idx)?.label
        )
    }

    pub fn selection(&self) -> Option<Rc<T>> {
        Some(
            self.items.get(self.selected_idx)?
                .val.clone()
        )
    }

    pub fn selection_mut(&mut self) -> Option<&mut T> {
        Rc::get_mut(
            &mut self.items.get_mut(self.selected_idx)?
                .val
        )
    }
}

impl<T: 'static> Widget for SelectWidget<T> {
    fn draw(&self, printer: &mut Printer) {
        let mut pos = PointVec::new(0, 0);
        let spacing = self.font_size as f64 / 5.;

        for (i, item) in self.items.iter().enumerate() {
            printer.draw_text(pos + (self.font_size as f64 * 1.5, 0), &item.label);
            if i == self.selected_idx && self.focused {
                printer.draw_arrow(pos + (spacing, spacing), self.font_size as f64, true, printer.theme.palette[PaletteColor::Highlight]);
            }

            let text_size = item.size(printer.font_cache);
            pos.y += text_size.y;
        }
    }

    fn on_event(&mut self, event: Event, _: &mut G2dTextureContext, _: FontCacheRef) -> EventResult {
        match event {
            Event::Key(key) if self.focused && !self.items.is_empty() => {
                let mut result = EventResult::Ignored;
                let mut exec_cb = false;

                if matches!(key, Key::Down | Key::Up) {
                    exec_cb = true;
                    result = EventResult::consumed();
                }

                match key {
                    Key::Down => {
                        if self.selected_idx < self.items.len() - 1 {
                            self.selected_idx += 1;
                        }
                        else {
                            self.selected_idx = self.items.len() - 1;
                            return EventResult::Ignored;
                        }
                    }

                    Key::Up => {
                        if self.selected_idx > 0 {
                            self.selected_idx -= 1;
                        }
                        else {
                            self.selected_idx = 0;
                            return EventResult::Ignored;
                        }
                    }

                    Key::Return => {
                        if let Some(callback) = self.on_submit.clone() {
                            let item = self.selection().unwrap();
                            result = EventResult::with_cb_once(move |ui| callback(ui, &item));
                        }
                    }

                    _ => { }
                }

                if exec_cb {
                    if let Some(callback) = self.on_select.clone() {
                        let item = self.selection().unwrap();
                        result = EventResult::with_cb_once(move |ui| callback(ui, &item));
                    }
                }

                result
            }

            Event::Focus(dir) if !self.items.is_empty() => {
                self.focused = true;
                match dir {
                    FocusDirection::Up => self.selected_idx = self.items.len() - 1,
                    FocusDirection::Down => self.selected_idx = 0,
                    _ => { }
                }

                if let Some(callback) = self.on_select.clone() {
                    let item = self.selection().unwrap();
                    EventResult::with_cb_once(move |ui| callback(ui, &item))
                }
                else { EventResult::consumed() }
            }

            Event::FocusLost => {
                self.focused = false;
                EventResult::consumed()
            }

            _ => EventResult::Ignored
        }
    }

    fn size(&self, max: PointVec, font_cache: FontCacheRef) -> PointVec {
        let sizes: Vec<PointVec> = self.items.iter()
            .map(|item| {
                let text_size = item.size(font_cache);

                if text_size.x > max.x && item.label.get_wrap_width().is_none() {
                    item.label.as_styled_str()
                        .wrap_width(*max.x)
                        .size(font_cache)
                }
                else {
                    text_size
                }
            })
            .collect();

        (
            sizes.iter()
                .map(|size| size.x)
                .max()
                .map(|val| *val)
                .unwrap_or(0.) +
                    (self.font_size as f64 * 1.5),

            sizes.iter()
                .map(|size| *size.y)
                .sum::<f64>()
        )
            .into()
    }

    fn can_focus(&self) -> bool { true }

    fn layout(&mut self, size: PointVec, _image_context: &mut G2dTextureContext, font_cache: FontCacheRef) {
        let max_width = size.x - (self.font_size as f64 * 1.5);
        for item in &mut self.items {
            let text_size = item.size(font_cache);
            if text_size.x > max_width {
                item.word_wrap_label(Some(*max_width));
            }
            else {
                item.word_wrap_label(None);
            }
        }
    }
}

new_default!(SelectWidget<T>);

struct Item<T: 'static> {
    val: Rc<T>,
    label: StyledString
}

impl<T: 'static> Item<T> {
    fn new(label: StyledString, val: T) -> Self {
        Item {
            val: Rc::new(val),
            label
        }
    }

    fn size(&self, font_cache: FontCacheRef) -> PointVec {
        self.label.size(font_cache)
    }

    fn word_wrap_label(&mut self, wrap_width: Option<f64>) {
        self.label.set_wrap_width(wrap_width);
    }
}