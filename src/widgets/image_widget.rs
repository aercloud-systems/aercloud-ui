use crate::{
    text::FontCache,
    vec::PointVec,
    Printer, Widget
};
use std::path::PathBuf;
use image::{
    imageops::FilterType,
    ImageReader
};
use piston_window::{
    TextureSettings,
    G2dTexture, G2dTextureContext,
    PistonWindow,
    Texture
};
use rust_utils::{chainable, encapsulated};

/// A widget that renders an image
#[encapsulated]
pub struct ImageWidget {
    #[getter(copy_val, doc = "Get the size of the [`ImageWidget`]")]
    size: PointVec,

    #[getter(doc = "Gets the path of the image")]
    path: PathBuf,

    img_buf: G2dTexture
}

impl ImageWidget {
    /// Create a new [`ImageWidget`] with a specific size
    pub fn new_sized<P: Into<PathBuf>, S: Into<PointVec>>(path: P, size: S, image_context: &mut G2dTextureContext) -> Self {
        let path = path.into();
        let size = size.into();
        let img = ImageReader::open(&path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(*size.x as u32, *size.y as u32, FilterType::Lanczos3);

        ImageWidget {
            size,
            img_buf: Texture::from_image(
                image_context,
                &img.to_rgba8(),
                &TextureSettings::new()
            ).unwrap(),
            path
        }
    }

    /// Create a new [`ImageWidget`]
    pub fn new<P: Into<PathBuf>>(path: P, image_context: &mut G2dTextureContext) -> Self {
        let path = path.into();
        let img = ImageReader::open(&path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap();

        let size = PointVec::new(img.width() as f64, img.height() as f64);

        ImageWidget {
            size,
            img_buf: Texture::from_image(
                image_context,
                &img.to_rgba8(),
                &TextureSettings::new()
            ).unwrap(),
            path
        }
    }

    /// Create a new [`ImageWidget`] using a [`PistonWindow`] texture context
    pub fn from_window<P: Into<PathBuf>>(path: P, window: &mut PistonWindow) -> Self {
        let mut img_ctx = window.create_texture_context();
        Self::new(path, &mut img_ctx)
    }

    /// Set the image to render
    #[chainable]
    pub fn set_image(&mut self, new_path: impl Into<PathBuf>, image_context: &mut G2dTextureContext) {
        let new_path = new_path.into();
        let raw_img = ImageReader::open(&new_path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(*self.size.x as u32, *self.size.y as u32, FilterType::Lanczos3);

        self.img_buf = Texture::from_image(image_context, &raw_img.to_rgba8(), &TextureSettings::new()).unwrap();
    }

    /// Set the rendering size of the image
    ///
    /// Size is measured in points
    pub fn set_size<S: Into<PointVec>>(&mut self, size: S, image_context: &mut G2dTextureContext) {
        self.size = size.into();
        let raw_img = ImageReader::open(&self.path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(*self.size.x as u32, *self.size.y as u32, FilterType::Lanczos3);

        self.img_buf = Texture::from_image(image_context, &raw_img.to_rgba8(), &TextureSettings::new()).unwrap();
    }
}

impl Widget for ImageWidget {
    fn draw(&self, printer: &mut Printer) {
        printer.draw_image((0., 0.), &self.img_buf);
    }

    fn size(&self, _: PointVec, _: &mut FontCache) -> PointVec { self.size }
}