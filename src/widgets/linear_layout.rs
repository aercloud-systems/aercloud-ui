use std::cell::Cell;
use piston_window::{G2dTextureContext, Key};
use rust_utils::{chainable, encapsulated};
use crate::{
    align::{
        Alignment,
        HAlignment,
        VAlignment
    },
    event::{
        Event,
        EventResult,
        FocusDirection
    },
    vec::{NotNan, PointVec},
    text::FontCacheRef,
    Printer, Widget
};

#[derive(PartialEq, Eq, Copy, Clone)]
enum Orientation {
    Horizontal,
    Vertical
}

impl Orientation {
    fn matches_axis(&self, event: Event) -> bool {
        if *self == Self::Horizontal {
            matches!(event, Event::Key(Key::Left | Key::Right))
        }
        else {
            matches!(event, Event::Key(Key::Up | Key::Down))
        }
    }
}

/// A widget that arranges its children linearly according to its orientation.
///
/// It is recommended to use the `hlayout!()` and `vlayout!()` macros instead of using
/// this directly
#[encapsulated]
pub struct LinearLayout {
    // is it vertical or horizontal?
    orientation: Orientation,

    // should it take up all the available space or just enough
    // for all the widgets to fit?
    #[getter(copy_val, doc = "Is this [`LinearLayout`] taking up all the available space?")]
    fill: bool,

    // index of the focused widget, if there is one
    focused_widget_idx: Option<usize>,

    // spacing of child widgets in points
    spacing: Cell<f64>,

    // the child widgets
    children: Vec<Child>,

    // the size since the last call to Widget::layout()
    size_cache: PointVec
}

impl LinearLayout {
    /// Creates a new vertical layout.
    pub fn vertical() -> Self {
        Self::new(Orientation::Vertical)
    }

    /// Creates a new horizontal layout.
    pub fn horizontal() -> Self {
        Self::new(Orientation::Horizontal)
    }

    fn new(orientation: Orientation) -> Self {
        LinearLayout {
            orientation,
            children: vec![],
            spacing: Cell::new(15.),
            fill: false,
            focused_widget_idx: None,
            size_cache: (0, 0).into()
        }
    }

    /// Set whether the layout takes up all available space or not
    #[chainable]
    pub fn set_fill(&mut self, fill: bool) {
        self.fill = fill;
        if !fill {
            self.spacing.set(15.);
        }
    }

    /// Adds a child to the layout.
    #[chainable]
    pub fn add_child(&mut self, widget: impl Widget) {
        self.children.push(Child::new(widget));
    }

    /// Inserts a child at the given position.
    ///
    /// # Panics
    ///
    /// Panics if `i > self.len()`.
    pub fn insert_child(&mut self, idx: usize, widget: impl Widget) {
        self.children.insert(idx, Child::new(widget));
        self.focused_widget_idx = None;
    }

    /// Swaps two children.
    pub fn swap_children(&mut self, idx1: usize, idx2: usize) {
        self.children.swap(idx1, idx2)
    }

    /// Returns a reference to a child.
    pub fn get_child(&self, idx: usize) -> Option<&dyn Widget> {
        self.children.get(idx)
            .map(|child| &*child.widget)
    }

    /// Returns a mutable reference to a child.
    pub fn get_child_mut(&mut self, idx: usize) -> Option<&mut dyn Widget> {
        self.children.get_mut(idx)
            .map(|child| &mut *child.widget)
    }

    /// Removes a child.
    pub fn remove_child(&mut self, idx: usize) {
        if idx < self.children.len() {
            self.children.remove(idx);
        }
    }

    /// Removes a child.
    ///
    /// If i is within bounds, the removed child will be returned.
    pub fn pop_child(&mut self, idx: usize) -> Option<Box<dyn Widget>> {
        if idx < self.children.len() {
            let child = self.children.remove(idx);
            Some(child.widget)
        }
        else { None }
    }

    /// Returns `true` if this view has no children.
    pub fn is_empty(&self) -> bool { self.children.is_empty() }

    /// Removes all children from this view.
    pub fn clear(&mut self) {
        self.children.clear();
        self.spacing.set(15.);
    }

    /// Returns the number of children.
    pub fn len(&self) -> usize { self.children.len() }

    // find a focusable child widget in the specfied focus direction
    // and return the result
    fn find_focused_widget(&mut self, dir: FocusDirection, image_context: &mut G2dTextureContext, font_cache: FontCacheRef) -> EventResult {
        // if there is an already focused child, find the next child to focus
        if let Some(focused_idx) = self.focused_widget_idx {
            let mut next_idx = if matches!(dir, FocusDirection::Down | FocusDirection::Right) {
                focused_idx + 1
            }
            else if focused_idx == 0 { 0 }
            else { focused_idx - 1 };

            // find the next child to focus
            while next_idx < self.children.len() {
                let child = &mut self.children[next_idx];

                // if one is found, focus it and return the result
                if child.can_focus() {
                    self.focused_widget_idx = Some(next_idx);
                    return child.on_event(Event::Focus(dir), self.size_cache, image_context, font_cache);
                }

                if matches!(dir, FocusDirection::Down | FocusDirection::Right) {
                    next_idx += 1;
                }
                else if matches!(dir, FocusDirection::Up | FocusDirection::Left) && next_idx == 0 {
                    break;
                }
                else if matches!(dir, FocusDirection::Up | FocusDirection::Left) {
                    next_idx -= 1;
                }
            }

            // otherwise if the current child can still
            // be focused, focus it in the opposite direction
            // and return its result
            let cur_focused_child = &mut self.children[focused_idx];
            if cur_focused_child.can_focus() {
                return cur_focused_child.on_event(Event::Focus(dir.opposite()), self.size_cache, image_context, font_cache);
            }
            else {
                self.focused_widget_idx = None;
            }
        }

        // if there are no focused children, find the first focusable child
        else {
            for (idx, child) in self.children.iter_mut().enumerate() {
                if child.can_focus() {
                    self.focused_widget_idx = Some(idx);
                    return child.on_event(Event::Focus(dir), self.size_cache, image_context, font_cache);
                }
            }

            self.focused_widget_idx = None;
        }

        // it seems none of the children can be focused
        EventResult::Ignored
    }
}

impl Widget for LinearLayout {
    fn draw(&self, printer: &mut Printer) {
        for child in &self.children {
            child.draw(printer);
        }
    }

    //TODO: reimplement how this widget handles resizing
    fn size(&self, max: PointVec, font_cache: FontCacheRef) -> PointVec {
        let mut area = max;
        let mut sizes = Vec::new();

        if self.spacing.get() < 0.  && !self.is_empty() {
            let div_area = if self.orientation == Orientation::Horizontal {
                PointVec::new(max.x / self.len() as f64, max.y)
            }
            else {
                PointVec::new(max.x, max.y / self.len() as f64)
            };

            for child in &self.children {
                child.set_size(div_area, font_cache);
                sizes.push(child.size());
            }

            self.spacing.set(0.);
        }
        else {
            for child in &self.children {
                child.set_size(area, font_cache);
                if self.orientation == Orientation::Horizontal {
                    area -= (child.size().x, 0);
                }
                else {
                    area -= (0, child.size().y);
                };

                sizes.push(child.size());
            }
            
            if self.fill {
                self.spacing.set(
                    if self.orientation == Orientation::Horizontal {
                        *area.x
                    }
                    else {
                        *area.y
                    } /
                        (self.children.len() + 1) as f64
                );
            }
        }

        let mut spacing = self.spacing.get();
        if spacing < 0. {
            spacing = 0.;
        }

        if self.orientation == Orientation::Horizontal {
            let max_height = sizes.iter()
                .map(|size| size.y)
                .max()
                .unwrap_or(if self.fill { max.y } else { 0.into() });

            if self.fill {
                PointVec::new(max.x, max_height)
            }
            else {
                let mut width: NotNan<f64> = sizes.iter()
                    .map(|size| size.x + spacing)
                    .sum();

                width += spacing;
                PointVec::new(width, max_height)
            }
        }
        else {
            let max_width = sizes.iter()
                .map(|size| size.x)
                .max()
                .unwrap_or(if self.fill { max.x } else { 0.into() });

            if self.fill {
                PointVec::new(max_width, max.y)
            }
            else {
                let mut height: NotNan<f64> = sizes.iter()
                    .map(|size| size.y + spacing)
                    .sum();

                height += spacing;
                PointVec::new(max_width, height)
            }
        }
    }

    fn layout(&mut self, size: PointVec, image_context: &mut G2dTextureContext, font_cache: FontCacheRef) {
        self.size_cache = size;
        let mut spacing = self.spacing.get();
        if spacing < 0. {
            spacing = 0.;
        }

        if self.orientation == Orientation::Horizontal {
            let mut widget_x = spacing;

            for child in &mut self.children {
                child.align = Alignment::new()
                    .h_align(HAlignment::Absolute(widget_x));

                widget_x += *child.size().x + spacing;
            }
        }
        else {
            let mut widget_y = spacing;

            for child in &mut self.children {
                child.align = Alignment::new()
                    .v_align(VAlignment::Absolute(widget_y));

                widget_y += *child.size().y + spacing;
            }
        }

        for child in &mut self.children {
            child.layout(image_context, font_cache);
        }
    }

    fn on_event(&mut self, event: Event, image_context: &mut G2dTextureContext, font_cache: FontCacheRef) -> EventResult {
        if event == Event::Refresh {
            // send the refresh event to all the widgets
            let mut cumul_result = EventResult::Ignored;

            for child in &mut self.children {
                cumul_result = cumul_result.and(
                    child.on_event(event, self.size_cache, image_context, font_cache)
                );
            }

            return cumul_result;
        }

        let mut focus_loss_result = EventResult::Ignored;

        // if there is a focused widget, send the event to it
        if let Some(focused_idx) = self.focused_widget_idx {
            let focused_child = &mut self.children[focused_idx];
            let result = focused_child.on_event(event, self.size_cache, image_context, font_cache);

            // if the event isn't an arrow key event, no need to take away focus
            if result.is_consumed() || !event.is_arrow_key() || !self.orientation.matches_axis(event) {
                return result;
            }

            // if it ignores the event, it loses focus
            focus_loss_result = focused_child.on_event(Event::FocusLost, self.size_cache, image_context, font_cache);
        }

        // go to the next focusable widget with the arrow keys
        let result = match event {
            Event::Key(key) => {
                let maybe_dir = match key {
                    Key::Down => Some(FocusDirection::Down),
                    Key::Up => Some(FocusDirection::Up),
                    Key::Left => Some(FocusDirection::Left),
                    Key::Right => Some(FocusDirection::Right),
                    _ => None
                };

                if let Some(dir) = maybe_dir {
                    self.find_focused_widget(dir, image_context, font_cache)
                }
                else { EventResult::Ignored }
            }

            Event::Focus(dir) => self.find_focused_widget(dir, image_context, font_cache),
            _ => EventResult::Ignored
        };

        result.and(focus_loss_result)
    }

    fn can_focus(&self) -> bool {
        self.children.iter()
            .any(|child| child.can_focus())
    }
}

struct Child {
    widget: Box<dyn Widget>,
    size: Cell<PointVec>,
    align: Alignment
}

impl Child {
    fn new<W: Widget>(widget: W) -> Self {
        Child {
            widget: Box::new(widget),
            size: Cell::new(PointVec::new(0., 0.)),
            align: Alignment::new()
        }
    }

    fn set_size(&self, max: PointVec, font_cache: FontCacheRef) {
        let size = self.widget.size(max, font_cache);
        self.size.set(size);
    }

    fn size(&self) -> PointVec { self.size.get() }

    fn draw(&self, printer: &mut Printer) {
        self.widget.draw_aligned(self.align, printer);
    }

    fn on_event(&mut self, event: Event, parent_size: PointVec, image_context: &mut G2dTextureContext, font_cache: FontCacheRef) -> EventResult {
        self.widget.on_event(event.mouse_offset(self.align, parent_size, &*self.widget, font_cache), image_context, font_cache)
    }

    fn can_focus(&self) -> bool { self.widget.can_focus() }

    fn layout(&mut self, image_context: &mut G2dTextureContext, font_cache: FontCacheRef) {
        self.widget.layout(self.size(), image_context, font_cache)
    }
}