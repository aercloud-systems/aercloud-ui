//! # Aercloud UI
//! Aercloud UI is an Open GL (using piston) based GUI that
//! uses a callback system similar to the one used in [the cursive TUI library](https://docs.rs/cursive/latest/cursive/)
//! 
//! This was originally created to clean up the code in [2048 Vanced](https://gitlab.com/NoahJelen/2048-vanced)

/// The underlying `piston-window` crate has been re-exported
/// for your convenience
pub use piston_window;

/// Various widgets to use when creating the layout.
pub mod widgets;

/// Tools to control widget alignment.
pub mod align;

/// User-input events and their effects.
pub mod event;

/// Points on a 2D grid
pub mod vec;

/// Tools to manipulated formatted text
pub mod text;

/// Handle colors and themes in the UI.
pub mod theme;

#[cfg(test)]
mod tests;

mod printer;
mod widget;
mod ui;
mod macros;

pub use self::{
    printer::Printer,
    widget::Widget,
    ui::AercloudUI
};