use rust_utils::{new_default, chainable, New};

/// Color presets
pub mod colors {
    pub use piston_window::color::{
        BLACK,
        BLUE,
        CYAN,
        GRAY,
        GREEN,
        LIME,
        MAGENTA,
        MAROON,
        NAVY,
        OLIVE,
        PURPLE,
        RED,
        SILVER,
        TEAL,
        WHITE,
        YELLOW,
        TRANSPARENT
    };
}

mod palette;

pub use palette::*;

/// Represents a primitive color used by Aercloud UI
pub use piston_window::types::Color as PrimitiveColor;

/// Represents the style an Aercloud UI application will use
#[derive(Clone, Debug, New)]
#[chainable]
pub struct Theme {
    /// Should certain widgets have shadows?
    #[chainable(doc = "Should certain widgets have shadows?")]
    pub shadow: bool,

    /// How view borders should be drawn
    #[chainable(doc = "How view borders should be drawn")]
    pub borders: BorderStyle,

    /// What colors should be used throughout the application?
    #[chainable(doc = "What colors should be used throughout the application?")]
    pub palette: Palette,
}

impl Default for Theme {
    fn default() -> Self {
        Theme {
            shadow: true,
            borders: BorderStyle::Simple,
            palette: Palette::new()
        }
    }
}

/// Color style for a span of text
#[derive(Copy, Clone)]
#[chainable]
pub struct ColorStyle {
    /// Front (character) color
    #[chainable(doc = "Front (character) color")]
    pub front: Color,

    /// Background color
    #[chainable(doc = "Background color")]
    pub back: Color
}

impl ColorStyle {
    /// Color a new white on transparent [`ColorStyle`]
    pub fn new() -> Self {
        ColorStyle {
            front: Color::Light(BaseColor::White),
            back: Color::Transparent
        }
    }
}

impl From<(Color, Color)> for ColorStyle {
    fn from(val: (Color, Color)) -> Self {
        ColorStyle {
            front: val.0,
            back: val.1
        }
    }
}

impl From<Color> for ColorStyle {
    fn from(val: Color) -> Self {
        ColorStyle {
            front: val,
            back: Color::Transparent
        }
    }
}

new_default!(ColorStyle);

/// Represents a color used by Aercloud UI
///
/// The alpha channel controls the opacity of the color
#[derive(Copy, Clone, Debug)]
pub enum Color {
    /// The absence of color and an alpha channel
    Transparent,

    /// One of the 8 base colors.
    Dark(BaseColor),

    /// Lighter version of a base color.
    Light(BaseColor),

    /// 24-bit color with an alpha channel.
    RGBA(u8, u8, u8, u8)
}

impl Color {
    /// Return the color with a new alpha channel value
    pub fn alpha(self, alpha: u8) -> Self {
        match self {
            Self::Transparent => Self::RGBA(0, 0, 0, alpha),
            Self::Dark(basic_color) => {
                match basic_color {
                    BaseColor::Black => Self::RGBA(0, 0, 0, alpha),
                    BaseColor::Red => Self::RGBA(128, 0, 0, alpha),
                    BaseColor::Green => Self::RGBA(0, 128, 0, alpha),
                    BaseColor::Yellow => Self::RGBA(128, 128, 0, alpha),
                    BaseColor::Blue => Self::RGBA(0, 0, 128, alpha),
                    BaseColor::Magenta => Self::RGBA(128, 0, 128, alpha),
                    BaseColor::Cyan => Self::RGBA(0, 128, 128, alpha),
                    BaseColor::White => Self::RGBA(192, 192, 192, alpha)
                }
            }

            Self::Light(basic_color) => {
                match basic_color{
                    BaseColor::Black => Self::RGBA(128, 128, 128, alpha),
                    BaseColor::Red => Self::RGBA(255, 0, 0, alpha),
                    BaseColor::Green => Self::RGBA(0, 255, 0, alpha),
                    BaseColor::Yellow => Self::RGBA(255, 255, 0, alpha),
                    BaseColor::Blue => Self::RGBA(0, 0, 255, alpha),
                    BaseColor::Magenta => Self::RGBA(255, 0, 255, alpha),
                    BaseColor::Cyan => Self::RGBA(0, 255, 255, alpha),
                    BaseColor::White => Self::RGBA(255, 255, 255, alpha)
                }
            }

            Self::RGBA(r, g, b, _) => Color::RGBA(r, g, b, alpha)
        }
    }

    /// 8-Bit color
    pub fn from_256_colors(col: u8) -> Self {
        let col_int = col as u16;
        let r = (col_int >> 5) * 32;
        let g = ((col_int & 28) >> 2) * 32;
        let b = (col_int & 3) * 64;
        Color::RGBA(r as u8, g as u8, b as u8, 255)
    }
}

impl From<Color> for PrimitiveColor {
    fn from(color: Color) -> Self {
        match color {
            Color::Transparent => colors::TRANSPARENT,
            Color::Dark(basic_color) => {
                match basic_color {
                    BaseColor::Black => colors::BLACK,
                    BaseColor::Red => colors::MAROON,
                    BaseColor::Green => colors::GREEN,
                    BaseColor::Yellow => colors::OLIVE,
                    BaseColor::Blue => colors::NAVY,
                    BaseColor::Magenta => colors::PURPLE,
                    BaseColor::Cyan => colors::TEAL,
                    BaseColor::White => colors::SILVER
                }
            }

            Color::Light(basic_color) => {
                match basic_color {
                    BaseColor::Black => colors::GRAY,
                    BaseColor::Red => colors::RED,
                    BaseColor::Green => colors::LIME,
                    BaseColor::Yellow => colors::YELLOW,
                    BaseColor::Blue => colors::BLUE,
                    BaseColor::Magenta => colors::MAGENTA,
                    BaseColor::Cyan => colors::CYAN,
                    BaseColor::White => colors::WHITE
                }
            }

            Color::RGBA(r, g, b, a) => [
                r as f32 / 255.,
                g as f32 / 255.,
                b as f32 / 255.,
                a as f32 / 255.
            ]
        }
    }
}

/// The 8 basic computer colors
#[derive(Copy, Clone, Debug)]
pub enum BaseColor {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White
}

/// Specifies how some borders should be drawn
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum BorderStyle {
    /// Simple borders.
    Simple,

    /// Outset borders with a simple 3d effect
    Outset,

    /// No borders.
    None
}

impl BorderStyle {
    /// Returns an iterator on all possible border styles
    pub fn values() -> impl Iterator<Item = Self> {
        [Self::Simple, Self::Outset, Self::None].into_iter()
    }
}

impl<S: AsRef<str>> From<S> for BorderStyle {
    fn from(string: S) -> Self {
        if string.as_ref() == "simple" {
            BorderStyle::Simple
        }
        else if string.as_ref() == "outset" {
            BorderStyle::Outset
        }
        else {
            BorderStyle::None
        }
    }
}