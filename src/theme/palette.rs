use super::{Color, BaseColor};
use std::{
    error::Error,
    str::FromStr,
    collections::HashMap,
    ops::{Index, IndexMut},
    fmt::{
        Debug, Display,
        Formatter,
        Result as FmtResult
    }
};
use rust_utils::{chainable, hash_map, new_default};

/// Color entry in a palette.
///
/// Each [`PaletteColor`] is used for a specific role in a default application.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum PaletteColor {
    /// Color used for the application background.
    Background,

    /// Color used for widget shadows.
    Shadow,

    /// Color used for widget backgrounds.
    Widget,

    /// Primary color used for the text.
    Primary,

    /// Secondary color used for the text.
    Secondary,

    /// Tertiary color used for the text.
    Tertiary,

    /// Primary color used for title text.
    TitlePrimary,

    /// Secondary color used for title text.
    TitleSecondary,

    /// Color used for highlighting text.
    Highlight,

    /// Color used for highlighting inactive text.
    HighlightInactive,

    /// Color used for highlighted text
    HighlightText
}

impl FromStr for PaletteColor {
    type Err = NoSuchColor;

    fn from_str(string: &str) -> Result<Self, NoSuchColor> {
        Ok(
            match string {
                "Background" | "background" => PaletteColor::Background,
                "Shadow" | "shadow" => PaletteColor::Shadow,
                "Widget" | "widget" => PaletteColor::Widget,
                "Primary" | "primary" => PaletteColor::Primary,
                "Secondary" | "secondary" => PaletteColor::Secondary,
                "Tertiary" | "tertiary" => PaletteColor::Tertiary,
                "TitlePrimary" | "title_primary" => PaletteColor::TitlePrimary,
                "TitleSecondary" | "title_secondary" => PaletteColor::TitleSecondary,
                "Highlight" | "highlight" => PaletteColor::Highlight,
                "HighlightInactive" | "highlight_inactive" => PaletteColor::HighlightInactive,
                "HighlightText" | "highlight_text" => PaletteColor::HighlightText,
                _ => return Err(NoSuchColor)
            }
        )
    }
}

/// Error parsing a palette color from a string
pub struct NoSuchColor;

impl Debug for NoSuchColor {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "Unable to parse color!")
    }
}

impl Display for NoSuchColor {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{self:?}")
    }
}

impl Error for NoSuchColor { }

/// Color configuration for the application.
///
/// Assign each color role an actual color.
///
/// It implements `Index` and `IndexMut` to access and modify this mapping
#[derive(Clone)]
pub struct Palette {
    basic: HashMap<PaletteColor, Color>,
    custom: HashMap<String, Color>
}

impl Palette {
    /// Create a new palette with the default theme
    pub fn new() -> Self {
        Palette {
            basic: hash_map! {
                PaletteColor::Background => Color::Transparent.alpha(192),
                PaletteColor::Primary => Color::Light(BaseColor::White),
                PaletteColor::Widget => Color::Transparent,
                PaletteColor::Highlight => Color::Light(BaseColor::Blue),
                PaletteColor::HighlightText => Color::Dark(BaseColor::White),
                PaletteColor::Secondary => Color::Dark(BaseColor::Blue),
                PaletteColor::TitlePrimary => Color::Light(BaseColor::Blue),
                PaletteColor::TitleSecondary => Color::Dark(BaseColor::Blue),
                PaletteColor::Shadow => Color::Dark(BaseColor::Black),
                PaletteColor::Tertiary => Color::Light(BaseColor::White),
                PaletteColor::HighlightInactive => Color::Dark(BaseColor::Blue)
            },

            custom: HashMap::new()
        }
    }

    /// Create a color palette with a ncurses style theme
    pub fn new_classic() -> Self {
        let mut palette = Self::new();
        palette[PaletteColor::Background] = Color::Dark(BaseColor::Blue);
        palette[PaletteColor::Primary] = Color::Dark(BaseColor::Black);
        palette[PaletteColor::Widget] = Color::Dark(BaseColor::White);
        palette[PaletteColor::Highlight] = Color::Dark(BaseColor::Red);
        palette[PaletteColor::HighlightText] = Color::Dark(BaseColor::White);
        palette[PaletteColor::Secondary] = Color::Dark(BaseColor::Blue);
        palette[PaletteColor::TitlePrimary] = Color::Dark(BaseColor::Red);
        palette[PaletteColor::TitleSecondary] = Color::Dark(BaseColor::Yellow);
        palette
    }

    /// Get a basic palette color
    pub fn get_color(&self, p_color: PaletteColor) -> Color {
        self.basic.get(&p_color).copied().unwrap()
    }

    /// Set a basic palette color
    #[chainable]
    pub fn set_color(&mut self, p_color: PaletteColor, color: Color) {
        self.basic.insert(p_color, color);
    }

    /// Get a custom palette color
    ///
    /// if `key` can be parsed into a [`PaletteColor`], it is used instead
    pub fn get_custom_color<T:AsRef<str>>(&self, key: T) -> Option<Color> {
        if let Ok(p_color) = key.as_ref().parse::<PaletteColor>() {
            Some(self.get_color(p_color))
        }
        else {
            self.custom.get(key.as_ref()).copied()
        }
    }

    /// Set a custom palette color
    ///
    /// if `key` can be parsed into a [`PaletteColor`], it is used instead
    #[chainable]
    pub fn set_custom_color<T: AsRef<str>>(&mut self, key: T, color: Color) {
        if let Ok(p_color) = key.as_ref().parse::<PaletteColor>() {
            self.set_color(p_color, color)
        }
        else {
            self.custom.insert(key.as_ref().to_string(), color);
        }
    }
}

impl Index<PaletteColor> for Palette {
    type Output = Color;

    #[track_caller]
    fn index(&self, palette_color: PaletteColor) -> &Color {
        &self.basic[&palette_color]
    }
}

impl IndexMut<PaletteColor> for Palette {
    #[track_caller]
    fn index_mut(&mut self, palette_color: PaletteColor) -> &mut Color {
        self.basic.get_mut(&palette_color).unwrap()
    }
}

impl<T: AsRef<str>> Index<T> for Palette {
    type Output = Color;

    #[track_caller]
    fn index(&self, key: T) -> &Color {
        if let Ok(p_color) = key.as_ref().parse::<PaletteColor>() {
            &self[p_color]
        }
        else {
            &self.custom[key.as_ref()]
        }
    }
}

impl<T: AsRef<str>> IndexMut<T> for Palette {
    #[track_caller]
    fn index_mut(&mut self, key: T) -> &mut Color {
        if let Ok(p_color) = key.as_ref().parse::<PaletteColor>() {
            &mut self[p_color]
        }
        else {
            self.custom.get_mut(key.as_ref()).unwrap()
        }
    }
}

new_default!(Palette);

impl Debug for Palette {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        for (p_color, color) in self.basic.iter() {
            write!(f, "{p_color:?} => {color:?}")?;
        }

        for (key, color) in self.custom.iter() {
            write!(f, "\"{key}\" => {color:?}")?;
        }

        Ok(())
    }
}