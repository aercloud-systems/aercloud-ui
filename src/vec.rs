use std::{
    fmt::{
        Debug,
        Formatter,
        Result  as FmtResult
    },
    ops::{
        Add, AddAssign,
        Div, DivAssign,
        Mul, MulAssign,
        Sub, SubAssign
    }
};
use piston_window::Size;
use winit::dpi::PhysicalSize;

/// An orderable float
///
/// Can be dereferenced to access the underlying float value
pub use ordered_float::NotNan;

/// A 2D vector in points (a 2 element [`f64`] (or any type that can be converted to it) array)
pub type RawPointVec<T = f64> = [T; 2];

/// A 2D vector in points
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct PointVec {
    /// The X value
    pub x: NotNan<f64>,

    /// The Y value
    pub y: NotNan<f64>
}

impl PointVec {
    /// Create a new [`PointVec`]
    ///
    /// Panics if the float value isn't a number
    pub fn new<X: Into<f64>, Y: Into<f64>>(x: X, y: Y) -> Self {
        PointVec {
            x: NotNan::new(x.into()).unwrap(),
            y: NotNan::new(y.into()).unwrap()
        }
    }
}

impl Debug for PointVec {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}, {}", self.x, self.y)
    }
}

impl<T: Into<f64>> From<RawPointVec<T>> for PointVec {
    fn from([x, y]: RawPointVec<T>) -> Self {
        PointVec::new(x, y)
    }
}

impl<X: Into<f64>, Y: Into<f64>> From<(X, Y)> for PointVec {
    fn from((x, y): (X, Y)) -> Self {
        Self::new(x, y)
    }
}

impl From<Size> for PointVec {
    fn from(val: Size) -> Self {
        let raw: RawPointVec = val.into();
        Self::from(raw)
    }
}

impl<T: Into<f64>> From<PhysicalSize<T>> for PointVec {
    fn from(val: PhysicalSize<T>) -> Self {
        Self::new(
            val.width,
            val.height
        )
    }
}

impl<P: Into<PointVec>> Add<P> for PointVec {
    type Output = PointVec;

    fn add(self, rhs: P) -> PointVec {
        let rhs = rhs.into();
        Self::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl<P: Into<PointVec>> AddAssign<P> for PointVec {
    fn add_assign(&mut self, rhs: P) {
        *self = *self + rhs;
    }
}

impl<P: Into<PointVec>> Sub<P> for PointVec {
    type Output = PointVec;

    fn sub(self, rhs: P) -> PointVec {
        let rhs = rhs.into();
        Self::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl<P: Into<PointVec>> SubAssign<P> for PointVec {
    fn sub_assign(&mut self, rhs: P) {
        *self = *self - rhs;
    }
}

/// [`PointVec`]s can be multiplied by scalars
impl Mul<f64> for PointVec {
    type Output = PointVec;
    fn mul(self, rhs: f64) -> Self::Output {
        Self::new(self.x * rhs, self.y * rhs)
    }
}

impl MulAssign<f64> for PointVec {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs;
    }
}

/// [`PointVec`]s can be divided by scalars
impl Div<f64> for PointVec {
    type Output = PointVec;
    fn div(self, rhs: f64) -> Self::Output {
        Self::new(self.x / rhs, self.y / rhs)
    }
}

impl DivAssign<f64> for PointVec {
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs;
    }
}

impl From<PointVec> for Size {
    fn from(val: PointVec) -> Self {
        Size {
            width: *val.x,
            height: *val.y
        }
    }
}

impl From<PointVec> for RawPointVec {
    fn from(val: PointVec) -> Self {
        [val.x.into(), val.y.into()]
    }
}