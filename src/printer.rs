use std::f64::consts::PI;
use gfx_device_gl::Device;
use piston_window::{
    draw_state::Stencil,
    ellipse::Border as EllipseBorder,
    rectangle::Border as RectBorder,
    CircleArc, Context,
    DrawState, Ellipse,
    G2d, G2dTexture,
    Image, Line,
    Polygon as PistonPolygon,
    Rectangle, Transformed
};
use crate::{
    text::{
        FontCacheRef,
        StyledStr
    },
    theme::{
        colors::*,
        BaseColor,
        Color,
        PrimitiveColor,
        Theme
    },
    vec::{PointVec, RawPointVec}
};

const DEBUG_BORDER_COLORS: [PrimitiveColor; 15] = [
    MAROON,
    RED,
    OLIVE,
    YELLOW,
    LIME,
    GREEN,
    CYAN,
    TEAL,
    BLUE,
    NAVY,
    PURPLE,
    MAGENTA,
    GRAY,
    SILVER,
    WHITE,
];

/// A convenient interface that allows drawing in an area of
/// the window
pub struct Printer<'p, 'g, 'd> {
    /// The currently loaded font
    /// 
    /// This can be used to do text related operations,
    /// like finding the point width
    /// of some text.
    pub font_cache: FontCacheRef<'p>,

    /// The size of the drawing area
    pub size: PointVec,

    /// The offset of the drawing area from the top left of the window
    pub offset: PointVec,

    /// The application's current theme
    pub theme: &'p Theme,

    pub (super) ctx: Context,
    pub (super) gfx: &'p mut G2d<'g>,
    pub (super) dev: &'d mut Device,
    pub (super) stencil_lvl: u8,
    pub (super) debug: bool
}

impl<'p, 'g, 'd> Printer<'p, 'g, 'd> {
    pub (super) fn add_offset<O: Into<PointVec>>(&mut self, new_offset: O) {
        self.offset += new_offset;
    }

    pub (super) fn set_offset<O: Into<PointVec>>(&mut self, new_offset: O) {
        self.offset = new_offset.into();
    }

    pub (super) fn set_size<S: Into<PointVec>>(&mut self, new_size: S) {
        self.size = new_size.into();
    }

    /// Set the background color of the widget area
    pub fn set_background(&mut self, color: Color) {
        self.draw_box((0, 0), self.size, color, None);
    }

    /// Draws an image
    pub fn draw_image<P: Into<PointVec>>(&mut self, pos: P, img_buf: &G2dTexture) {
        let state = self.get_draw_state();
        let (x, y) = self.get_ofs_coords(pos);
        Image::new()
            .draw(img_buf, &state, self.ctx.transform.trans(x, y), self.gfx);
    }

    /// Draw a rectangle with an optional border
    pub fn draw_box<P: Into<PointVec>, S: Into<PointVec>>(&mut self, pos: P, size: S, color: Color, border_color: Option<Color>) {
        let state = self.get_draw_state();
        let color: PrimitiveColor = color.into();
        let size = size.into();
        let (x, y) = self.get_ofs_coords(pos);
        let mut rect = Rectangle::new(color);

        if let Some(color) = border_color {
            let color: PrimitiveColor = color.into();
            rect = rect.border(
                RectBorder {
                    color,
                    radius: 1.
                }
            )
        }

        rect.draw([x, y, size.x.into(), size.y.into()], &state, self.ctx.transform, self.gfx);
    }

    /// Draw an arrow
    pub fn draw_arrow<P: Into<PointVec>, H: Into<f64>>(&mut self, pos: P, height: H, right: bool, color: Color) {
        let height = height.into();
        let pos = pos.into();
        
        if right {
            self.draw_polygon(
                [
                    pos,
                    pos + (0, height),
                    pos + ((height / 2., height / 2.))
                ],
                color
            );
        }
        else {
            self.draw_polygon(
                [
                    pos + (height / 2., 0),
                    pos + (height / 2., height),
                    pos + (0, height / 2.)
                ],
                color
            );
        }
    }

    /// Draw text
    pub fn draw_text<'t, T: Into<StyledStr<'t>>, P: Into<PointVec>>(&mut self, pos: P, text: T) {
        text.into()
            .draw(self.get_ofs_coords(pos).into(), self)
    }

    /// Draws an arc with the specified rectangular size and curvature in degrees
    /// 
    /// any `curve_len` value > 360 will just draw a full circle / ellipse
    pub fn draw_arc<P: Into<PointVec>, S: Into<PointVec>>(&mut self, pos: P, size: S, line_weight: f64, mut curve_start: f64, mut curve_len: f64, color: Color) {
        let state = self.get_draw_state();
        let pos = pos.into();
        let size = size.into();
        let (x, y) = self.get_ofs_coords(pos);
        if curve_len > 360. { curve_len = 360.; }
        if curve_start > 360. { curve_start = 360.; }
        let calc_rads = |deg| (deg * PI) / 180.;
        let arc = CircleArc::new(
            color.into(),
            line_weight,
            calc_rads(curve_start),
            calc_rads(curve_start + curve_len)
        );

        arc.draw([x, y, size.x.into(), size.y.into()], &state, self.ctx.transform, self.gfx)
    }

    /// Draw an ellipse with the specified rectangular size
    pub fn draw_ellipse<P: Into<PointVec>, S: Into<PointVec>>(&mut self, pos: P, size: S, color: Color, border_color: Option<Color>) {
        let state = self.get_draw_state();
        let (x, y) = self.get_ofs_coords(pos);
        let size = size.into();
        let color: PrimitiveColor = color.into();
        let mut oval = Ellipse::new(color);

        if let Some(color) = border_color {
            let color: PrimitiveColor = color.into();
            oval = oval.border(
                EllipseBorder {
                    color,
                    radius: 1.
                }
            )
        }

        oval.draw([x, y, size.x.into(), size.y.into()], &state, self.ctx.transform, self.gfx);
    }

    /// Draw a line from one point to another
    pub fn draw_line<A: Into<PointVec>, B: Into<PointVec>>(&mut self, from: A, to: B, line_weight: f64, color: Color) {
        let state = self.get_draw_state();
        let (x1, y1) = self.get_ofs_coords(from);
        let (x2, y2) = self.get_ofs_coords(to);
        Line::new(color.into(), line_weight)
            .draw_from_to([x1, y1], [x2, y2], &state, self.ctx.transform, self.gfx);
    }

    /// Draw a filled polygon from a set of points
    pub fn draw_polygon<Polygon: IntoIterator<Item = P> + Clone, P: Into<PointVec>>(&mut self, polygon: Polygon, color: Color) {
        let state = self.get_draw_state();
        let points: Vec<RawPointVec> = polygon.into_iter()
            .map(|point| {
                let (x, y) = self.get_ofs_coords(point);
                [x, y]
            })
            .collect();

        PistonPolygon::new(color.into())
            .draw(&points, &state, self.ctx.transform, self.gfx)
    }

    fn get_ofs_coords<P: Into<PointVec>>(&self, pos: P) -> (f64, f64) {
        let pos = self.offset + pos;
        (pos.x.into(), pos.y.into())
    }

    pub (crate) fn get_draw_state(&self) -> DrawState {
        DrawState {
            stencil: Some(Stencil::Inside(self.stencil_lvl)),
            ..DrawState::default()
        }
    }

    pub (crate) fn prep_stencil_area(&mut self) {
        let color = Color::Light(BaseColor::White);
        Rectangle::new(color.into())
            .draw(
                [
                    *self.offset.x,
                    *self.offset.y,
                    *self.size.x,
                    *self.size.y
                ],
                &DrawState::new_increment(),
                self.ctx.transform,
                self.gfx
            );

        if self.debug {
            Rectangle::new_border(DEBUG_BORDER_COLORS[(self.stencil_lvl % 15) as usize], 1.)
                .draw(
                    [
                        *self.offset.x,
                        *self.offset.y,
                        *self.size.x,
                        *self.size.y
                    ],
                    &self.get_draw_state(),
                    self.ctx.transform,
                    self.gfx
                );
        }
    }
}

impl<'p, 'g, 'd> Drop for Printer<'p, 'g, 'd> {
    fn drop(&mut self) {
        self.font_cache.flush_buffer(self.dev);
    }
}