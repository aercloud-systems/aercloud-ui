use std::path::{Path, PathBuf};
use glutin_window::{
    GlutinWindow, OpenGL,
    UserEvent
};
use piston_window::{
    clear, image as draw_image,
    Button, ButtonEvent,
    Context, EventLoop,
    G2d, G2dTexture,
    G2dTextureContext,
    MouseButton,
    MouseCursorEvent,
    MouseScrollEvent,
    PistonWindow,
    TextureSettings,
    ResizeEvent, Texture,
    Window, AdvancedWindow,
    WindowSettings
};
use rust_utils::{chainable, encapsulated};
use winit::{
    window::{Icon, WindowBuilder},
    dpi::LogicalSize,
    event_loop::EventLoopBuilder
};
use image::{
    imageops::FilterType,
    ImageReader
};
use crate::{
    event::{
        Event,
        EventResult,
        FocusDirection,
        MouseEvent
    },
    theme::{Color, Theme},
    align::Alignment,
    text::FontCache,
    vec::PointVec,
    Printer, Widget
};

#[cfg(test)]
use winit::platform::x11::EventLoopBuilderExtX11;

/// The core of Aercloud UI. It handles all the the window events. It can and should be populated with
/// widgets to be useful.
///
/// The event loop is started by using `run()`
#[encapsulated]
pub struct AercloudUI {
    window: PistonWindow,
    image_context: G2dTextureContext,
    font_cache: Option<FontCache>,
    layers: Vec<AlignedWidget>,
    backdrop: Option<Backdrop>,
    should_quit: bool,

    #[getter(copy_val, doc = "Is debug mode enabled?")]
    #[setter(doc = "Enable / Disable debug mode")]
    #[chainable]
    debug: bool,

    #[getter(mutable, doc = "The current app theme")]
    #[setter(doc = "Set the app theme")]
    #[chainable]
    theme: Theme
}

impl AercloudUI {
    /// Create a new [`AercloudUI`] root and initialize the window
    pub fn new(title: &str) -> AercloudUI {
        let window = genwin::<&str>(title, None, 200, 200);
        Self::from(window)
    }

    /// Same as new() but with a custom icon
    pub fn new_with_icon<IP: AsRef<Path>>(title: &str, icon_path: IP) -> AercloudUI {
        let window = genwin(title, Some(icon_path), 200, 200);
        Self::from(window)
    }

    /// Set the font used by [`AercloudUI`]
    ///
    /// The default is Noto Sans Regular which is normally
    /// preinstalled on most Linux systems
    #[chainable]
    pub fn set_font(&mut self, font_name: &str) {
        self.font_cache = Some(FontCache::load(&mut self.window, font_name));
    }

    /// Set the font used by [`AercloudUI`] from a path
    ///
    /// The default is Noto Sans Regular which is normally
    /// preinstalled on most Linux systems
    ///
    /// Panics if the font cannot be found
    #[chainable]
    pub fn set_font_from_path<P: AsRef<Path>>(&mut self, font_path: P) {
        self.font_cache = Some(FontCache::load_from_path(&mut self.window, font_path));
    }

    /// Set the backdrop image used by [`AercloudUI`]
    #[chainable]
    pub fn set_backdrop_image<P: AsRef<Path>>(&mut self, path: P) {
        self.backdrop = Some(Backdrop::new_image(path, self.window.size().into(), &mut self.image_context))
    }

    /// Set the backdrop color used by [`AercloudUI`]
    #[chainable]
    pub fn set_backdrop_color(&mut self, color: Color) {
        self.backdrop = Some(Backdrop::new_color(self.window.size().into(), color));
    }

    /// Add a layer to the window
    /// 
    /// Widgets are centered by default
    #[chainable]
    pub fn add_layer<W: Widget>(&mut self, widget: W) {
        self.add_aligned_layer(widget, Alignment::new());
    }

    /// Add a layer to the window with
    /// a custom alignment
    #[chainable]
    pub fn add_aligned_layer<W: Widget>(&mut self, widget: W, align: Alignment) {
        self.layers.push(
            AlignedWidget {
                align,
                widget: Box::new(widget)
            }
        );
    }

    /// Remove the topmost layer
    pub fn remove_top(&mut self) { self.pop_top(); }

    /// Remove the topmost layer and return it if there is one
    pub fn pop_top(&mut self) -> Option<Box<dyn Widget>> {
        self.layers.pop()
            .map(|al_widget| al_widget.widget)
    }

    /// Close the window
    pub fn quit(&mut self) { self.should_quit = true; }

    /// Start the event loop
    pub fn run(&mut self) {
        if self.font_cache.is_none() {
            self.set_font("Noto Sans")
        }

        self.should_quit = false;

        // set the initial window size
        let monitor = self.window.window.window.current_monitor().unwrap();
        let monitor_size: PointVec = monitor.size().into();
        let init_size = monitor_size * 0.75;

        // layout the widgets for the first time
        let mut layer_sizes = Vec::new();
        for AlignedWidget { widget, .. } in &mut self.layers {
            let font_cache = self.font_cache.as_mut().unwrap();
            let size = widget.size(init_size.into(), font_cache);
            widget.layout(size, &mut self.image_context, font_cache);
            layer_sizes.push(size);
        }

        // now set the window size to the largest layer size
        let largest_size = layer_sizes.iter().copied().max().unwrap();
        self.window.set_size(largest_size);

        // size the backdrop for the first time
        if let Some(backdrop) = &mut self.backdrop {
            backdrop.resize(largest_size, &mut self.image_context);
        }

        // position of mouse cursor in window
        let mut mouse_pos = [0., 0.];

        // the current mouse button being held down
        let mut cur_mouse_button: Option<MouseButton> = None;

        // has a key been pressed?
        // prevents keyboard events from being sent twice
        let mut pressed_key = false;

        // the current window size
        let mut win_size: PointVec = self.window.size().into();

        if let Some(AlignedWidget { widget, .. }) = self.layers.last_mut() {
            let font_cache = self.font_cache.as_mut().unwrap();
            let result = widget.on_event(Event::Focus(FocusDirection::Right), &mut self.image_context, font_cache);
            if let EventResult::Consumed(Some(callback)) = result {
                callback(self)
            }
        }

        while let Some(piston_event) = self.window.next() {
            // when all is said and done, close the window
            if self.should_quit {
                self.window.set_should_close(true);
                break;
            }

            // when the window resizes, pass the new size info to all the widgets
            // and backdrop
            if let Some(resize_event) = piston_event.resize_args() {
                let font_cache = self.font_cache.as_mut().unwrap();
                win_size = resize_event.window_size.into();
                if let Some(backdrop) = &mut self.backdrop {
                    backdrop.resize(win_size, &mut self.image_context);
                }

                for AlignedWidget { widget, .. } in &mut self.layers {
                    let size = widget.size(win_size, font_cache);
                    widget.layout(size, &mut self.image_context, font_cache);
                }
            }

            // process an event and send it to the topmost widget
            let event = if let Some(new_mouse_pos) = piston_event.mouse_cursor_args() {
                cur_mouse_button = None;

                // update cursor position when it moves
                if mouse_pos != new_mouse_pos {
                    mouse_pos = new_mouse_pos;
                    Event::Mouse {
                        offset: (0., 0.).into(),
                        pos: mouse_pos.into(),
                        event: MouseEvent::CursorMoved
                    }
                }
                else { Event::Refresh }
            }

            // is the user scrolling?
            else if let Some([_, v_dir]) = piston_event.mouse_scroll_args() {
                Event::Mouse {
                    offset: (0., 0.).into(),
                    pos: mouse_pos.into(), 
                    event: if v_dir == 1. {
                        MouseEvent::WheelUp
                    }
                    else {
                        MouseEvent::WheelDown
                    }
                }
            }

            // mouse or keyboard button was pressed
            else if let Some(button_args) = piston_event.button_args() {
                if let Button::Mouse(mouse_button) = button_args.button {
                    // mouse button was released
                    if let Some(cur_button) = cur_mouse_button {
                        if cur_button == mouse_button {
                            cur_mouse_button = None;
                            Event::Mouse {
                                offset: (0., 0.).into(),
                                pos: mouse_pos.into(),
                                event: MouseEvent::Release(mouse_button)
                            }
                        }
                        else {
                            cur_mouse_button = Some(mouse_button);
                            Event::Mouse {
                                offset: (0., 0.).into(),
                                pos: mouse_pos.into(),
                                event: MouseEvent::Press(mouse_button)
                            }
                        }
                    }

                    // mouse click
                    else {
                        cur_mouse_button = Some(mouse_button);
                        Event::Mouse {
                            offset: (0., 0.).into(),
                            pos: mouse_pos.into(),
                            event: MouseEvent::Press(mouse_button)
                        }
                    }
                }

                // keyboard press
                else if let Button::Keyboard(key) = button_args.button {
                    cur_mouse_button = None;
                    if !pressed_key {
                        pressed_key = true;
                        Event::Key(key)
                    }
                    else {
                        pressed_key = false;
                        Event::Refresh
                    }
                }

                else { Event::Refresh }
            }

            // is a mouse button being held down?
            else if let Some(cur_button) = cur_mouse_button {
                Event::Mouse {
                    offset: (0., 0.).into(),
                    pos: mouse_pos.into(),
                    event: MouseEvent::Hold(cur_button)
                }
            }

            else { Event::Refresh };

            /*#[cfg(test)]
            if !matches!(event, Event::Refresh) {
                println!("{event:?}");
            }*/

            // process the events of the topmost widget
            if let Some(AlignedWidget { widget, .. }) = self.layers.last_mut() {
                let font_cache = self.font_cache.as_mut().unwrap();
                let result = widget.on_event(event, &mut self.image_context, font_cache);
                if let EventResult::Consumed(Some(callback)) = result {
                    callback(self)
                }
            }

            // render the window
            self.window.draw_2d(&piston_event, |ctx, gfx, dev| {
                clear([0., 0., 0., 0.5], gfx);
                let mut printer = Printer {
                    size: win_size,
                    offset: PointVec::new(0., 0.),
                    theme: &self.theme,
                    ctx,
                    gfx,
                    font_cache: self.font_cache.as_mut().unwrap(),
                    dev,
                    stencil_lvl: 0,
                    debug: self.debug
                };
                
                // draw the backdrop if there is one
                if let Some(backdrop) = &self.backdrop {
                    backdrop.draw(ctx, printer.gfx)
                }

                // draw each layer
                for AlignedWidget { align, widget } in &self.layers {
                    widget.draw_aligned(*align, &mut printer);
                }
            });
        }
    }

    /// [`ImageWidget`]: crate::widgets::ImageWidget
    /// Get the image context for rendering images
    /// 
    /// This is needed for creating instances of [`ImageWidget`]
    pub fn img_ctx(&mut self) -> &mut G2dTextureContext { &mut self.image_context }

    /// Convert this `AercloudUI` instance back into the underlying [`PistonWindow`]
    pub fn into_window(self) -> PistonWindow { self.window }
}

impl From<PistonWindow> for AercloudUI {
    /// Create a new [`AercloudUI`] root from an already initialized window
    fn from(mut window: PistonWindow) -> Self {
        let image_context = window.create_texture_context();

        AercloudUI {
            window,
            image_context,
            font_cache: None,
            layers: vec![],
            backdrop: None,
            should_quit: false,
            theme: Theme::default(),
            debug: false
        }
    }
}

impl From<AercloudUI> for PistonWindow {
    fn from(ui: AercloudUI) -> Self {
        ui.into_window()
    }
}

struct AlignedWidget {
    align: Alignment,
    widget: Box<dyn Widget>
}

enum BackdropType {
    SolidColor(Color),
    Image(PathBuf, G2dTexture)
}

struct Backdrop {
    bd_type: BackdropType,
    win_size: PointVec
}

impl Backdrop {
    fn new_image<P: AsRef<Path>>(path: P, win_size: PointVec, image_context: &mut G2dTextureContext) -> Self {
        let img = ImageReader::open(&path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(*win_size.x as u32, *win_size.y as u32, FilterType::Lanczos3);

        Backdrop {
            bd_type: BackdropType::Image(
                path.as_ref().to_path_buf(),
                Texture::from_image(
                    image_context,
                    &img.to_rgba8(),
                    &TextureSettings::new()
                )
                    .unwrap()
            ),
            win_size
        }
    }

    fn new_color(win_size: PointVec, color: Color) -> Self {
        Backdrop {
            bd_type: BackdropType::SolidColor(color),
            win_size
        }
    }

    fn resize(&mut self, new_size: PointVec, tc: &mut G2dTextureContext) {
        if let BackdropType::Image(ref path, ref mut img_buf) = self.bd_type {
            let img = ImageReader::open(&path).unwrap()
                .with_guessed_format().unwrap()
                .decode().unwrap()
                .resize_exact(*new_size.x as u32, *new_size.y as u32, FilterType::Lanczos3);

            *img_buf = Texture::from_image(
                tc,
                &img.to_rgba8(),
                &TextureSettings::new()
            ).unwrap();
        }

        self.win_size = new_size;
    }

    fn draw(&self, ctx: Context, gfx: &mut G2d) {
        clear([0.; 4], gfx);
        match self.bd_type {
            BackdropType::Image(_, ref img_buf) => draw_image(img_buf, ctx.transform, gfx),
            BackdropType::SolidColor(color) => clear(color.into(), gfx)
        }
    }
}

// generates the window with its icon
fn genwin<P: AsRef<Path>>(title: &str, icon_path: Option<P>, width: u32, height: u32) -> PistonWindow {
    // event loop
    #[cfg(not(test))]
    let eloop = EventLoopBuilder::<UserEvent>::with_user_event().build();

    #[cfg(test)]
    let eloop = {
        let mut eloop_build = EventLoopBuilder::<UserEvent>::with_user_event();
        eloop_build.with_any_thread(true);
        eloop_build.build()
    };

    // the raw winit window
    let mut win_raw = WindowBuilder::new().with_resizable(true).with_inner_size(LogicalSize::new(width, height));

    if let Some(path) = icon_path {
        // the icon image
        let icon_image = ImageReader::open(path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(256, 256, FilterType::Lanczos3);

        let image_bytes = icon_image.as_bytes();
        let icon = Icon::from_rgba(image_bytes.to_vec(), 256, 256).unwrap();
        win_raw = win_raw.with_window_icon(Some(icon))
    }

    // the window's settings
    let win_cfg = WindowSettings::new(title.to_string(), [width, height]);

    // create the window
    let gl_win = GlutinWindow::from_raw(&win_cfg, eloop, win_raw.with_title(title.to_string())).unwrap();

    PistonWindow::new(OpenGL::V4_5, 0, gl_win).lazy(false)
}