#[macro_export]
macro_rules! hlayout {
    ($($widget:expr), *) => {
        $crate::widgets::LinearLayout::horizontal()
            $( .child($widget) )*
    };
}

#[macro_export]
macro_rules! vlayout {
    ($($widget:expr), *) => {
        $crate::widgets::LinearLayout::vertical()
            $( .child($widget) )*
    };
}